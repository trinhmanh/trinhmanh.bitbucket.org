/**
 * Created by LongLV on 9/22/2016.
 */
(function (namespace) {
    namespace.ChannelDTO = function () {

    };

    namespace.ChannelDTO.prototype = {
        channelId: "",
        title: "",
        className: "",
        imgURL: "",
        createdDate: ""
    };
})(window.FREQ);