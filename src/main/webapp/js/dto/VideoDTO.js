/**
 * Created by DungLA4 on 9/19/2016.
 * Video Object to transfer between app flow. Need to convert from response data
 */
(function (namespace) {
    namespace.VideoDTO = function () {

    };

    namespace.VideoDTO.prototype = {
        videoId: "",
        title: "",
        videoURL: "",
        imgURL: "",
        datePublish: "",
        channelId: "",
        channelTitle: ""
    };
})(window.FREQ);