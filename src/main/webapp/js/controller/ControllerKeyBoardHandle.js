(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    namespace.addEvent(document, 'keydown', keyBoardEvenHandler);
    function keyBoardEvenHandler(evt) {
        if (window.dialog.isShow()) {
            window.dialog.processKeyBoardEvent(evt.keyCode);
        	evt.preventDefault();
        	return true;
        }
        //namespace.debug(evt.keyCode + " " + evt.key);
        switch (evt.keyCode) {
            case window.VK_RIGHT:
                if (window.dialog.isShow()) {
                    window.dialog.processKeyBoardEvent(window.VK_RIGHT);
                    return true;
                }
                rightContent.processKeyBoardEvent(window.VK_RIGHT);

                break;
            case window.VK_LEFT:
                if(!leftMenu.isActive()) {
                    rightContent.processKeyBoardEvent(window.VK_LEFT);
                } 
                break;
            case window.VK_UP:
                if(leftMenu.isActive()) {
                    leftMenu.setNewSelectedItem(window.VK_UP);
                } else {
                    rightContent.processKeyBoardEvent(window.VK_UP);
                }
                break;
            case window.VK_DOWN:
                if(leftMenu.isActive()) {
                    leftMenu.setNewSelectedItem(window.VK_DOWN);
                } else {
                    rightContent.processKeyBoardEvent(window.VK_DOWN);
                }
                break;
            case window.VK_ENTER:
                if (window.dialog.isShow()) {
                    window.dialog.processKeyBoardEvent(window.VK_ENTER);
                    return true;
                }
                if(leftMenu.isActive()) {
                    rightContent.processKeyBoardEvent(window.VK_RIGHT);
                } else {
                    rightContent.processKeyBoardEvent(window.VK_ENTER);
                }
                break;
            case window.VK_BACK:
                if(leftMenu.isActive()) {
                    dialog.confirmDialog("Exit Frequency", "Are you sure you want to exit Frequency?", "Exit", closeWindow);
                } else {
                    rightContent.processKeyBoardEvent(window.VK_BACK);
                }
                break;
            case window.VK_PLAY:
            case window.VK_PAUSE:
            case window.VK_FORWARD:
            case window.VK_BACKWARD:
            case window.VK_LIKE:
            case window.VK_UNLIKE:
                if (!leftMenu.isActive()) {
                    rightContent.processKeyBoardEvent(evt.keyCode);
                }
                break;
        }
        evt.preventDefault();
    }
    function closeWindow() {
        window.close();
    }
})(window.FREQ)