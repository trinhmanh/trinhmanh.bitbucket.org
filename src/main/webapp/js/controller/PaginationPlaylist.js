(function (namespace) {
    namespace.PaginationPlaylist = function (dom) {
        this.dom = (dom == undefined) ? "#related-video-section > ul" : dom;
    };

    namespace.PaginationPlaylist.prototype = {
        playlist: [],
        selectedIndex: 0,
        length: 0,
        leftWindowIndex: 0,
        rightWindowIndex: 0,
        leftBeltIndex: 0,
        rightBeltIndex: 0,
        distanceBeltWindow: 2,
        pageSize: 10,
        domSize: 7,
        pageOffset: 0,
        urlRequest: "",
        dom: "",
        currentPlayingIndex: 0,
        appendData: function (videos, pageOffset) {
            if (videos && videos.length > 0) {
                if (this.playlist.length == 0) {
                    // First time append data, need to set all indexes
                    this.setRightWindowIndex((videos.length > 4) ? 4 : videos.length);
                    this.setLeftWindowIndex(-1);
                    this.selectedIndex = 0;
                }
                this.pageOffset = pageOffset;
                //this.playlist.push(...videos);
                for (var i = 0; i < videos.length; i++) {
                    this.playlist.push(this.convertJsonToObjectStore(videos[i]));
                }
            }
            namespace.log("After appending data, we have " + this.playlist.length + " videos in list");

        },
        clearData: function () {
            this.playlist.length = 0;
            this.leftBeltIndex = this.leftWindowIndex = this.rightBeltIndex = this.rightWindowIndex = this.selectedIndex = -1;
            this.currentPlayingIndex = 0;
        },
        /**
         * Move selected item to right 1 item. Also move window to right 1 item if selected item is the last item of
         * window
         */
        moveSelectedToRight: function () {
            if (this.selectedIndex < this.playlist.length - 1) {
                this.selectedIndex++;
                // Normal flow: get next page when move window to right
                if (this.selectedIndex == this.rightWindowIndex) {
                    if (this.leftWindowIndex >= 0) {
                        var playlistElement = this.getDOMList();
                        window.renderHTML.insertDataToRelatedVideoHTML(playlistElement, this.playlist[this.selectedIndex + 1 + this.domSize - 6], true);
                        if (this.getDOMList().childElementCount > this.domSize) {
                            playlistElement.removeChild(playlistElement.childNodes[0]);
                        }
                    }
                    this.moveWindowToRight();
                }
            }
            this.log();
        },
        /**
         * Move window and belt to right 1 item. If right belt is the last item, request to append data to playlist
         * Return true if move successfully, otherwise return false
         */
        moveWindowToRight: function () {
            var success = false;
            if (this.rightWindowIndex < this.playlist.length - 1) {
                this.setLeftWindowIndex(this.leftWindowIndex + 1);
                this.setRightWindowIndex(this.rightWindowIndex + 1);
                if (this.rightBeltIndex == this.playlist.length - 1) {
                    // Belt at the last item
                    if (this.pageOffset != null) {
                        namespace.log("Currently we have " + this.playlist.length + " videos in list");
                        namespace.debug("Request loading..");
                        freService.getContentByURL(this.urlRequest + "&page_offset=" + this.pageOffset, this.getDataFromService);
                    } else {
                        namespace.log("No more videos from channel to load");
                    }
                }
                success = true;
            }
            return success;
        },
        moveSelectedToLeft: function () {
            if (this.selectedIndex > 0) {
                this.selectedIndex--;
                if (this.selectedIndex == this.leftWindowIndex) {
                    this.moveWindowToLeft();
                    // Delete the last item in view
                    var playlistElement = this.getDOMList();
                    var length = playlistElement.childElementCount;
                    playlistElement.removeChild(playlistElement.childNodes[length - 1]);
                    // Insert an item in the head of view
                    window.renderHTML.insertDataToRelatedVideoHTML(playlistElement, this.playlist[this.selectedIndex - 1], false);
                }
            }
            this.log();
        },
        /**
         * Move window and belt to left 1 item
         */
        moveWindowToLeft: function () {
            this.setLeftWindowIndex(this.leftWindowIndex - 1);
            this.setRightWindowIndex(this.rightWindowIndex - 1);
        },
        /**
         * Use to convert string response to array and store to pagination
         */
        getDataFromService: function (data) {
            data = JSON.parse(data);
            if (data && data.videos && data.videos.length > 0) {
                window.paginationPlaylist.appendData(data.videos, data.page.page_offset);
                namespace.debug("Loading done");
            }
        },
        setLeftWindowIndex: function (index) {
            this.leftWindowIndex = index;
            this.leftBeltIndex = index - this.distanceBeltWindow;
        },
        setRightWindowIndex: function (index) {
            this.rightWindowIndex = index;
            this.rightBeltIndex = index + this.distanceBeltWindow;
        },
        isLastItem: function () {
            return this.selectedIndex == this.playlist.length - 1;
        },
        isFirstItem: function () {
            return this.selectedIndex == 0;
        },
        isFirstItemPlaying: function () {
            return this.currentPlayingIndex == 0;
        },
        isLastItemPlaying: function () {
            return this.currentPlayingIndex == this.playlist.length - 1;
        },
        getSelectedIndexInWindow: function () {
            return (this.leftWindowIndex < 0) ? this.selectedIndex : this.selectedIndex - this.leftWindowIndex;
        },
        getDOMList: function () {
            return document.querySelector(this.dom);
        },
        setURLRequest: function (url, ...args) {
            if (url.match(/\?1/g)) {
                for (var i = 0; i < args.length; i++) {
                    url = url.replace("?" + (i + 1), args[i]);
                }
                this.urlRequest = url;
            } else {
                this.urlRequest = url;
            }
            this.urlRequest += "?page_size=" + this.pageSize;
        },
        log: function () {
            namespace.debug("Selected: " + this.selectedIndex + "/" + this.playlist.length + ", belt: " + this.leftBeltIndex + "/" + this.rightBeltIndex
                    + ", window: " + this.leftWindowIndex + "/" + this.rightWindowIndex);
        },
        convertJsonToObjectStore: function (data) {
            var obj = {
                video_id: data.video_id,
                title: data.title,
                channel: {
                    title: data.channel.title,
                    channel_id: data.channel.channel_id,
                    image_url: data.channel.image_url
                },
                image_url: data.image_url,
                media_url: data.media_url,
                source_id: data.source_id,
                source_url: data.source_url,
                date_publish: data.date_publish
            };
            return obj;
        },
        /**
         * Get next video in play list. No checking if current playing is the last video
         * @returns {*|Window.FREQ.VideoDTO}
         */
        getNextVideoDTO: function () {
            return namespace.convertResponseData2VideoDTO(this.playlist[this.currentPlayingIndex + 1]);
        },
        /**
         * Get previous in play list. No checking if current playing is the first video
         * @returns {*|Window.FREQ.VideoDTO}
         */
        getPreviousVideoDTO: function () {
            return namespace.convertResponseData2VideoDTO(this.playlist[this.currentPlayingIndex - 1]);
        },
        logPlaylist: function () {
            var videoIds = [];
            for (var i = 0; i < this.playlist.length; i++) {
                videoIds[i] = this.playlist[i].video_id;
            }
            console.log("Playlist: " + videoIds);
        },
        renderPlaylist: function () {
            var tmp = [], j = (this.currentPlayingIndex == 0) ? 0 : this.currentPlayingIndex - 1;
            for (var i = 0; i < this.domSize; i++) {
                tmp.push(this.playlist[j]);
                j++;
            }
            renderHTML.renderRelatedVideoListHTML(tmp, this.domSize);
            tmp = null;
        },
        setSelectedToCurrentPlaying: function () {
            this.selectedIndex = this.currentPlayingIndex;
            this.setLeftWindowIndex(this.currentPlayingIndex - 1);
            this.setRightWindowIndex(this.currentPlayingIndex + 4);
        }
    }

})(window.FREQ);