(function(namespace){
    /*
     * Author: LongLV
     * CreateDate: 24/08/2016
     */
    var videoSection = document.getElementById("video-player");
    var playButton = document.getElementById("play");
    var playerFrame = "#player";
    var playerWrapper = videoSection.querySelector(".player-wrapper");
    var playerControl = document.getElementById("player-control");
    var guide = document.getElementById("guide");
    var relatedVideo = document.getElementById("related-video-section");
    var information = document.getElementById("information");
    var videoTitle = document.querySelector('#information .mov-title');
    var videoDesc = document.querySelector('#information .mov-desc');
    var progressBar = document.querySelector('.meter').firstElementChild;
    var displayTime = document.getElementById('current-time');
    function getStateName(state) {
        var status;
        switch (state) {
            case -1:
                status = "UNSTARTED";
                break;
            case 0:
                status = "ENDED";
                break;
            case 1:
                status = "PLAYING";
                break;
            case 2:
                status = "PAUSED";
                break;
            case 3:
                status = "BUFFERING";
                break;
            case 4:
                status = "LOCKED";
                break;
            case 5:
                status = "SEEKING";
                break;
            case 6:
                status = "ERROR";
                break;
            default:
                status = "UNKNOWN";
        }
        return status;
    }

    /**
     * Make progress bar and display time consistent with current time
     */
    function setPlayerControlByCurrentTime(currentTime, duration) {
        //namespace.log("In setPlayerControlByCurrentTime(currentTime=" + currentTime + ", duration=" + duration);
        progressBar.style.width = (100*currentTime)/duration + '%';
        displayTime.innerHTML = namespace.convertSecondsToMinSec(currentTime);
    }
    namespace.PlayerTiVo = function() {
        var _this = this;
        window.player = new Frequency.Player(
            playerFrame, {
                session: {
                    'X-Frequency-Auth': '1111',
                    'X-Frequency-DeviceId': '22222'
                },
                events: {
                    onProgress: function(currentTime) {
                        _this.currentTime = Math.floor(currentTime);
                        //namespace.log("In onProgress event");
                        setPlayerControlByCurrentTime(_this.currentTime, Math.floor(window.player.getDuration()));
                    },
                    onError: function(error) {
                        console.log('onError', error)
                    },
                    onStateChange: function (state) {
                        var playerState = getStateName(state);
                        namespace.debug(playerState);
                        var displayTop = false,
                            displayBottom = false,
                            displayLoadingIcon = false,
                            needCorrectProgressBarAndTime = false;

                        // Control manually in the case PLAYING and SEEKING, BUFFERING (youtube) state because of limited library
                        if (playerState == "PLAYING") {
                            window.startVideo = false;
                            namespace.hiddenLoadingVideo();
                            _this.correctPlayButtonIcon(true);
                            return;
                        } else if (playerState == "SEEKING") {
                            _this.correctPlayButtonIcon(true);
                            return;
                        } else if (playerState == "BUFFERING" && rightContent.isFullScreenView()
                                && window.startVideo == false) {
                            // Prevent for youtube source (playerState is buffering while seeking)
                            _this.correctPlayButtonIcon(true);
                            return;
                        }

                        switch (playerState) {
                            case "PAUSED":
                                displayTop = displayBottom = true;
                                break;
                            case "ENDED":
                                displayTop = displayBottom = true;
                                needCorrectProgressBarAndTime = true;
                                break;
                            case "BUFFERING":
                                if (rightContent.isFullScreenView()) {
                                    if (window.startVideo == true) {
                                        displayLoadingIcon = true;
                                    }
                                }
                                break;
                            case "UNSTARTED":
                                if (rightContent.isFullScreenView()) {
                                    displayLoadingIcon = true;
                                }
                                break;
                        }

                        if (playerState == "ERROR") {
                            namespace.notify("Error while loading video");
                        } else {
                            _this.correctPlayButtonIcon(false);
                            _this.displayTop(displayTop);
                            _this.displayBottom(displayBottom);
                            if (displayLoadingIcon) {
                                namespace.loadingVideo();
                            } else {
                                namespace.hiddenLoadingVideo();
                            }
                            if (needCorrectProgressBarAndTime) {
                                setPlayerControlByCurrentTime(window.player.getDuration(), window.player.getDuration());
                            }
                        }

                    }
                }
            }
        );
    };
    namespace.PlayerTiVo.prototype = {
        video: {},
        currentTime: 0,
        displayAll: function(display) {
            this.displayTop(display);
            this.displayBottom(display);
        },
        /**
         * Display top area while pausing. It contains guide and video playlist
         * @param display
         */
        displayTop: function (display) {
            if (display == true) {
                namespace.addClass(guide, "display");
                namespace.addClass(relatedVideo, "display");
            } else {
                namespace.removeClass(guide, "display");
                namespace.removeClass(relatedVideo, "display");
            }
        },
        /**
         * Display bottom area while pausing. It contains player control and video information
         * @param display
         */
        displayBottom: function(display) {
            if (display == true) {
                namespace.addClass(playerWrapper, "display");
                namespace.addClass(playerControl, "display");
                namespace.addClass(information, "display");
            } else {
                namespace.removeClass(playerWrapper, "display");
                namespace.removeClass(playerControl, "display");
                namespace.removeClass(information, "display");
            }
        },
        /**
         * Display only player control then fade out after some seconds. Use while seeking
         */
        displayControlThenFadeOut: function () {
            playerControl.classList.add("display");
            namespace.restartAnimation(playerControl, "control-fade-out");
            playerControl.addEventListener('webkitAnimationEnd', function (e) {
                e.target.removeEventListener(e.type, arguments.callee);
                playerControl.classList.remove("control-fade-out");
                playerControl.classList.remove("display");
            });
        },
        correctPlayButtonIcon: function (forPlayingMode) {
            playButton.checked = !forPlayingMode;
        },
        /**
         * Not allow another seek action while seeking
         */
        seekTo: function(seconds) {
            this.currentTime = Math.floor(this.currentTime);
            var newTime = this.currentTime + seconds;
            var duration = Math.floor(window.player.getDuration());
            if (newTime >= duration) {
                newTime = window.player.getDuration();
            } else if (newTime < 0) {
                newTime = 0;
            }
            window.player.seekTo(newTime);
            this.currentTime = newTime;
            if (this.isPaused() || this.isEnded()) {
                setPlayerControlByCurrentTime(this.currentTime, duration);
            }
            if (this.isPlaying()) {
                this.displayControlThenFadeOut();
            }
        },
        forward: function (seconds) {
            if (seconds == undefined) {seconds = 5;}
            if (!this.isEnded()) {
                this.seekTo(seconds);
            }
        },
        backward: function (seconds) {
            if (seconds == undefined) {seconds = 5;}
            this.seekTo(-seconds);
            namespace.log(this.getPlayerStatus());
            if (this.isEnded()) {
                // Need to pause when backward at the end of video (fix for youtube)
                window.player.pause();
            }
        },
        generateVideoInfo: function(video) {
            var timeAgo = namespace.calculateTimeAgo(video.datePublish);
            videoTitle.innerHTML = video.title;
            videoDesc.innerHTML = video.channelTitle + ' ' + timeAgo;
        },
        /**
         * video param should be a VideoDTO, which has fields below:
         *  - videoId
         *  - videoURL
         *  - imgURL
         *  - title
         *  - datePublish
         *  - channelId
         *  - channelTitle
         */
        fullScreenViewByVideoDTO: function (video) {
            // window.startVideo is used to know video is seeking or starting
            window.startVideo = true;
            videoSection.style.display = "block";
            namespace.clearCurrentVideo();
            namespace.loadingVideo();
            if (video != null && video.videoURL) {
                this.video = video;
                this.playVideo(video.videoURL);
                this.generateVideoInfo(video);
            } else {
                window.dialog.showDialog("No movie response");
            }
        },
        playVideo: function(videoURL) {
            var videoType = namespace.getVideoType(videoURL);
            namespace.log(videoType, "VideoType");
            var opts = {autoplay: true};
            switch (videoType) {
                case MP4:
                    break;
                case YOUTUBE:
                    break;
                case HLS:
                    opts.hls = true;
                    break;
                case VIMEO:
                    break;
                case DAILY_MOTION:
                    break;
                case OTHER:
                    namespace.log("Unsupported format for URL", videoURL);
                    return;
            }
            opts.url = videoURL;
            window.player.loadVideo(opts);
        },
        exitFullScreen: function() {
            videoSection.style.display = "none";
            namespace.storeCurrentVideo(this.video.videoId, this.isPlaying());
            window.player.pause();
        },
        play: function () {
            this.displayAll(false);
            window.player.play();
        },
        pause: function () {
            this.currentTime = window.player.getCurrentTime();
            namespace.log("Pause video at time " + this.currentTime);
            namespace.stopAnimation(playerControl, "control-fade-out");
            window.player.pause();
            this.displayTop(true);
            if (!window.isPlayFromChannel && !window.isPlayFromChannelDetails) {
                renderHTML.renderRelatedVideoCarouselHTML();
            }
        },
        /* Toggle full screen mode of player */
        toggleFullScreenPlayer: function () {
            if (window.player.isFullscreen()) {
                window.player.exitFullScreen();
            } else {
                window.player.requestFullScreen();
            }
        },
        isPaused: function () {
            return window.player.getState() == 2;
        },
        isPlaying: function () {
            return window.player.getState() == 1;
        },
        isEnded: function () {
            return window.player.getState() == 0;
        },
        isBuffering: function () {
            return window.player.getState() == 3;
        },
        isSeeking: function () {
            return window.player.getState() == 5;
        },
        getPlayerStatus: function () {
            return getStateName(window.player.getState());
        }
    };
})(window.FREQ)