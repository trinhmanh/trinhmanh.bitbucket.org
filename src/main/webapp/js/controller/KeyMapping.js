/**
 * Created by DungLA4 on 9/14/2016.
 */
(function () {
    function isBrowserPC() {
        return window.innerWidth != 1920 && window.innerWidth != 1366 && window.innerWidth != 1280;
    }
    if (window.props.dev_mode) {
        // Keyboard setting
        window.VK_LEFT = 37;
        window.VK_RIGHT = 39;
        window.VK_UP = 38;
        window.VK_DOWN = 40;
        window.VK_ENTER = 13;                                   // Enter
        window.VK_BACK = isBrowserPC() ? 8 : 27;                // Backspace
        window.VK_PLAY = isBrowserPC() ? 79 : 415;              // O
        window.VK_PAUSE = isBrowserPC() ? 80 : 463;             // P
        window.VK_BACKWARD = isBrowserPC() ? 219 : 412;         // [
        window.VK_FORWARD =  isBrowserPC() ? 221 : 417;         // ]
        window.VK_LIKE = isBrowserPC() ? 76 : 429;              // L
        window.VK_UNLIKE = isBrowserPC() ? 85 : 437;            // U
    } else {
        // Remote button setting
        window.VK_LEFT = window.VK_LEFT || 37;
        window.VK_RIGHT = window.VK_RIGHT || 39;
        window.VK_UP = window.VK_UP || 38;
        window.VK_DOWN = window.VK_DOWN || 40;
        window.VK_ENTER = window.VK_ENTER || 13;
        window.VK_BACK = window.VK_BACK || 27;
        window.VK_PLAY = window.VK_PLAY || 415;
        window.VK_PAUSE = window.VK_PAUSE || 463;
        window.VK_BACKWARD = window.VK_BACKWARD || 412;
        window.VK_FORWARD = window.VK_FORWARD || 417;
        window.VK_LIKE = window.VK_LIKE || 429;
        window.VK_UNLIKE = window.VK_UNLIKE || 437;
    }
})(window.FREQ)