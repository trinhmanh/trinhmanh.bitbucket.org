/**
 * Created by LongLV on 9/16/2016.
 */
(function (namespace) {

    /*Define variable for this function*/
    var leftMenu = document.getElementById("left-navigation"),
        resultVideo = document.getElementById("list-video-section"),
        resultChannel = document.getElementById("list-channel-section"),
        firstLevelContent = document.querySelector(".first-level"),
        relatedVideoSection = document.getElementById('related-video-section');
    var detailSection = document.getElementById("channel-detail-section"),
        filterSection = document.getElementById("filter-channel");

    var videos = {};

    namespace.RenderHTML = function () {

    };
    namespace.RenderHTML.prototype = {
        renderLeftMenuHtml: function (data) {
            var listIcons = ["img/compass.png", "img/favorite.png", "img/channel.png", "img/search.png"];
            if (data && data.length > 0) {
                leftMenu.innerHTML = "";
                var i, leng = data.length;
                var docfrag = document.createDocumentFragment();
                for (i = 0; i < leng; i++) {
                    var divElement = document.createElement("div");
                    var imageElement = document.createElement("img");
                    var titleElement = document.createElement("div");
                    divElement.setAttribute("tab", "page-" + data[i].pageId.toLowerCase().replaceAll(" ", "_"));
                    divElement.setAttribute("class", "image-button");
                    if (i == 0) {
                        divElement.classList.add("active");
                    }
                    if (i > (listIcons.length - 1)) {
                        imageElement.setAttribute("src", listIcons[2]);
                    } else {
                        imageElement.setAttribute("src", listIcons[i]);
                    }
                    titleElement.setAttribute("class", "image-text");
                    titleElement.innerHTML = data[i].title;

                    divElement.appendChild(imageElement);
                    divElement.appendChild(titleElement);
                    docfrag.appendChild(divElement);
                }
                leftMenu.appendChild(docfrag);
            }
        },
        renderRightContentHTML: function (data) {
            // create all div for first level page
            if (data && data.length > 0) {
                firstLevelContent.innerHTML = "";
                var docfrag = document.createDocumentFragment();
                var i,idText, leng = data.length;
                for (i = 0; i < leng; i++) {
                    var divElement = document.createElement("div");
                    idText = "page-" + data[i].pageId.toLowerCase().replaceAll(" ", "_");
                    divElement.setAttribute("id", idText);
                    divElement.style.display = "none";
                    if (i == 0) {
                        divElement.style.display = "block"
                    }
                    docfrag.appendChild(divElement);
                }
                firstLevelContent.appendChild(docfrag);
                for (i = 0; i < leng; i++) {
                    idText = "page-" + data[i].pageId.toLowerCase().replaceAll(" ", "_");
                    if (i != 1 ) {
                        if (i == (leng - 1)) {
                            this.renderSearchField(idText);
                        }
                        this.renderLayoutByPage(idText, data[i].content);
                    } else {
                        this.renderInDevelopmentMessage(idText);
                    }
                }
            }
        },
        renderLayoutByPage: function (pageId, contentData) {
            var parentElement = document.getElementById(pageId);
            if (contentData && contentData.length > 0) {
                var i, leng = contentData.length;
                for (i = 0; i < leng; i++) {
                    var divElement = document.createElement("div");
                    var idText = contentData[i].rowId.toLowerCase().replaceAll(" ", "_");
                    divElement.setAttribute("id", idText);
                    parentElement.appendChild(divElement);
                    this.renderContent(idText, contentData[i].title, contentData[i].uri, contentData[i].className);
                }
            }
        },
        renderContent: function (sectionId, title, uri, className) {
            switch (className) {
                case "channels_promotional":
                    freService.getContentByURL(uri, renderHTML.renderChannelPromotion, sectionId, className);
                    break;
                case "videos":
                case "videos_management":
                    freService.getContentByURL(uri, renderHTML.renderVideoCarousel, sectionId, title, className);
                    break;
                case "channels":
                    freService.getContentByURL(uri, renderHTML.renderChannelCarousel, sectionId, title, className);
                    break;
                case "trending_topics":
                    freService.getContentByURL(uri, renderHTML.renderTrendingTopics, sectionId, title, className);
                    break;
                case "channels_special":
                    freService.getContentByURL(uri, renderHTML.renderFeaturedChannel, sectionId, className);
                    break;
                case "categories":
                    freService.getContentByURL(uri, renderHTML.renderCategoriesHTML, sectionId, title, className);
                    break;
            }
        },
        renderInDevelopmentMessage: function (sectionId) {
            var parentElement = document.getElementById(sectionId);
            var divMessage = document.createElement("div");
            var textMessage = document.createElement("h3");
            textMessage.style.textAlign = "left";
            textMessage.style.paddingLeft = "150px";
            textMessage.style.paddingTop = "40px";
            textMessage.style.fontSize = "20px";
            textMessage.innerHTML = "Sorry, this section is under development!";
            divMessage.appendChild(textMessage);
            parentElement.appendChild(divMessage);
        },
        renderSearchField: function (pageId) {
            var characters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
            var numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
            var parentElement = document.getElementById(pageId);
            var container = document.createElement("div");

            container.setAttribute("class", "search-view");

            var inputField = document.createElement("ul");
            namespace.addClass(inputField, "search-textfield");
            var liInput = document.createElement("li");
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("class", "search-input");
            input.setAttribute("placeholder", "Search Name, Title, Keyword");

            liInput.appendChild(input);
            inputField.appendChild(liInput);

            var characterField = document.createElement("ul");
            characterField.setAttribute("class", "option-search");
            var frag = document.createDocumentFragment();
            var lengText = characters.length;
            for (var i = 0; i < lengText; i++) {
                var spanText = document.createElement("span");
                spanText.innerHTML = characters[i];
                spanText.setAttribute("class", "character-item");
                spanText.setAttribute("key-value", characters[i]);
                frag.appendChild(spanText);
            }
            var liText = document.createElement("li");
            namespace.addClass(liText, "text-label");
            var spaceIcon = document.createElement("span");
            var deleteIcon = document.createElement("span");
            namespace.addClass(spaceIcon, "space-item");
            namespace.addClass(deleteIcon, "delete-item");
            spaceIcon.setAttribute("key-value", " ");
            deleteIcon.setAttribute("key-value", "delete");
            liText.appendChild(spaceIcon);
            liText.appendChild(frag);
            liText.appendChild(deleteIcon);
            characterField.appendChild(liText);

            var lengNumber = numbers.length;
            var fragNumber = document.createDocumentFragment();
            for (var i = 0; i < lengNumber; i++) {
                var spanText = document.createElement("span");
                spanText.innerHTML = numbers[i];
                spanText.setAttribute("class", "number-item");
                spanText.setAttribute("key-value", numbers[i]);
                fragNumber.appendChild(spanText);
            }
            var liNumber = document.createElement("li");
            namespace.addClass(liNumber, "number-label");
            liNumber.appendChild(fragNumber);
            characterField.appendChild(liNumber);

            var liButton = document.createElement("li");
            namespace.addClass(liButton, "button-label");
            var spanButton = document.createElement("span");
            spanButton.innerHTML = "SEARCH";
            spanButton.setAttribute("class", "button-item");
            spanButton.setAttribute("key-value", "search");
            liButton.appendChild(spanButton);
            characterField.appendChild(liButton);

            container.appendChild(inputField);
            container.appendChild(characterField);

            parentElement.appendChild(container);
        },
        renderChannelPromotion: function (data, sectionId, className) {
            var parentElement = document.getElementById(sectionId);
            parentElement.setAttribute("class", "carousel-section");
            parentElement.style.height = "260px";
            data = JSON.parse(data);
            if (data && data.length > 0) {
                var i, leng = data.length;
                var divElement = document.createElement("ul");
                divElement.setAttribute("class", "carousel-container carousel " + className);
                var docfrag = document.createDocumentFragment();
                for (i = 0; i < leng; i++) {
                    var carouselItem = document.createElement("li");
                    carouselItem.setAttribute("class", "big-carousel-item");
                    carouselItem.setAttribute("data-id", data[i].channel_id);
                    carouselItem.setAttribute("data-type", rightContent.CONTENT_TYPE[1]);
                    var imageElement = document.createElement("img");
                    imageElement.setAttribute("src", data[i].image_url);
                    carouselItem.appendChild(imageElement);
                    carouselItem.appendChild(renderHTML.createOverlayElement(data[i].title, data[i].short_title, null, null, "big"));
                    docfrag.appendChild(carouselItem);
                }
                divElement.appendChild(docfrag);
                parentElement.appendChild(divElement);
            }
        },
        renderFeaturedChannel: function (data, sectionId, className) {
            var parentElement = document.getElementById(sectionId);
            parentElement.setAttribute("class", "carousel-section");
            parentElement.style.height = "210px";
            data = JSON.parse(data);
            if (data && data.length > 0) {
                var i, leng = data.length;
                var divElement = document.createElement("ul");
                divElement.setAttribute("class", "carousel-container carousel " + className);
                var docfrag = document.createDocumentFragment();
                for (i = 0; i < leng; i++) {
                    var carouselItem = document.createElement("li");
                    carouselItem.setAttribute("class", "medium-channel-item");
                    carouselItem.setAttribute("data-id", data[i].channel_id);
                    carouselItem.setAttribute("data-type", rightContent.CONTENT_TYPE[1]);
                    var imageElement = document.createElement("img");
                    imageElement.setAttribute("src", data[i].image_url);
                    carouselItem.appendChild(imageElement);
                    docfrag.appendChild(carouselItem);
                }
                divElement.appendChild(docfrag);
                parentElement.appendChild(divElement);
            }
        },
        renderCategoriesHTML: function (data, sectionId, title, className) {
            var parentElement = document.getElementById(sectionId);
            var rootElement = parentElement.parentNode;
            parentElement.setAttribute("class", "carousel-section");
            var titleElement = document.createElement("h3");
            titleElement.setAttribute("class", "group-category-title");
            titleElement.innerHTML = title;
            rootElement.insertBefore(titleElement, parentElement);
            var data = JSON.parse(data);
            var page_size = data.page.page_size;
            if (page_size != null && page_size > 0) {
                var i;
                var rowElement, arr = data.categories, count = 0;
                var docfrag = document.createDocumentFragment();
                var optionfrag = document.createDocumentFragment();
                for (i = 0; i < page_size; i++) {
                    if ((i % 5) == 0) {
                        rowElement = document.createElement("div");
                        rowElement.setAttribute("class", "category-section");
                        var childElement = document.createElement("ul");
                        childElement.setAttribute("class", "category-row carousel-container category");
                        rowElement.appendChild(childElement);
                        var cloneElement = parentElement.cloneNode();
                        cloneElement.setAttribute("id", sectionId + "-" + count);
                        cloneElement.appendChild(rowElement)
                        count++;
                        docfrag.appendChild(cloneElement);
                    }
                    var categoryRow = rowElement.querySelector(".category-row");
                    var itemImage = document.createElement("li");
                    itemImage.setAttribute("class", "container-genre");
                    var imageTag = document.createElement("img");
                    imageTag.setAttribute("class", "img-genre");
                    if (arr[i].image_url) {
                        imageTag.setAttribute("src", arr[i].image_url);
                    } else {
                        imageTag.setAttribute("src", "img/no-image-available.png");
                    }

                    var divTag = document.createElement("div");
                    divTag.setAttribute("class", "center");
                    divTag.innerHTML = arr[i].title;
                    itemImage.appendChild(imageTag);
                    itemImage.appendChild(divTag);
                    itemImage.setAttribute("data-id", arr[i].category_id);
                    itemImage.setAttribute("data-type", rightContent.CONTENT_TYPE[0]);
                    categoryRow.appendChild(itemImage);
                    var optionTag = document.createElement("option");
                    optionTag.innerHTML = arr[i].title;
                    optionTag.setAttribute("value", arr[i].category_id);
                    optionfrag.appendChild(optionTag);
                }
                filterSection.querySelector(".category-filter").appendChild(optionfrag);
                rootElement.appendChild(docfrag);
                rootElement.removeChild(parentElement);
            }
            namespace.unBlockUI();
        },
        renderChannelVideosCarousel: function (data, sectionId, className) {

        },
        renderChannelCarousel: function (data, sectionId, title, className) {
            var parentElement = document.getElementById(sectionId);
            parentElement.setAttribute("class", "carousel-section");
            parentElement.style.height = "180px";
            var titleElement = document.createElement("h3");
            titleElement.setAttribute("class", "carousel-title");
            titleElement.innerHTML = title;
            parentElement.appendChild(titleElement);
            data = JSON.parse(data);
            if (data && data.length > 0) {
                var i, leng = data.length;
                var divElement = document.createElement("ul");
                var docfrag = document.createDocumentFragment();
                divElement.setAttribute("class", "carousel-container carousel " + className);
                for (i = 0; i < leng; i++) {
                    var carouselItem = document.createElement("li");
                    carouselItem.setAttribute("class", "only-channel medium-carousel-item");
                    carouselItem.setAttribute("data-id", data[i].channel_id);
                    carouselItem.setAttribute("data-type", rightContent.CONTENT_TYPE[1]);
                    var imageElement = document.createElement("img");
                    imageElement.setAttribute("src", data[i].image_url);
                    carouselItem.appendChild(imageElement);
                    docfrag.appendChild(carouselItem);
                }
                divElement.appendChild(docfrag);
                parentElement.appendChild(divElement);
            }
        },
        renderChannelList: function (data, isAppend) {
            var data = JSON.parse(data);
            var page_size = data.page.page_size;
            if (page_size != null && page_size > 0) {
                var i;
                if (!isAppend) {
                    resultChannel.innerHTML = "";
                }
                var rowElement, arr = data.channels;
                resultChannel.setAttribute("data-page-size", data.page.page_size);
                resultChannel.setAttribute("data-page-offset", data.page.page_offset);
                resultChannel.setAttribute("data-page-mark", data.page.page_mark);
                var docfrag = document.createDocumentFragment();
                for (i = 0; i < page_size; i++) {
                    if ((i % 5) == 0) {
                        rowElement = document.createElement("ul");
                        rowElement.setAttribute("class", "array-container");
                        resultChannel.appendChild(rowElement);
                        docfrag.appendChild(rowElement);
                    }
                    var itemImage = document.createElement("li");
                    itemImage.setAttribute("class", "medium-channel-item");
                    itemImage.setAttribute("data-id", arr[i].channel_id);
                    itemImage.setAttribute("data-type", rightContent.CONTENT_TYPE[1]);
                    var imageTag = document.createElement("img");
                    if (arr[i].image_url) {
                        imageTag.setAttribute("src", arr[i].image_url);
                    } else {
                        imageTag.setAttribute("src", "img/no-image-available.png");
                    }
                    itemImage.appendChild(imageTag);
                    rowElement.appendChild(itemImage);
                }
                resultChannel.appendChild(docfrag);
            } else {
                resultChannel.innerHTML = "No results Found";
                namespace.addClass(document.querySelector(".dropdown-group"), "active");
                channelFilter.setSelectedFilter(channelFilter.filterType[0]);
                channel.filterState = true;
            }
            channel.displayChannelResult();
            namespace.hiddenLoadingVideo();
            namespace.unBlockUI();
        },
        renderVideosList: function (data, isAppend) {
            data = JSON.parse(data);
            var page_size = data.page.page_size;
            if (page_size != null && page_size > 0) {
                var i;
                if (!isAppend) {
                    resultVideo.innerHTML = "";
                    channelDetail.videoList = [];
                    channelDetail.indexEnd = 0;
                }
                detailSection.setAttribute("data-page-size", data.page.page_size);
                detailSection.setAttribute("data-page-offset", data.page.page_offset);
                detailSection.setAttribute("data-page-mark", data.page.page_mark);
                var rowElement, arr = data.videos;
                channelDetail.videoList.push(...arr);

                // Add to pagination list
                window.paginationPlaylist.appendData(data.videos, data.page.page_offset);

                for (i = 0; i < page_size; i++) {
                    if ((i % 4) == 0) {
                        rowElement = document.createElement("ul");
                        rowElement.setAttribute("class", "array-container");
                        resultVideo.appendChild(rowElement);
                    }
                    var itemImage = document.createElement("li");
                    itemImage.setAttribute("class", "medium-video-item");
                    itemImage.setAttribute("data-index", channelDetail.indexEnd);
                    itemImage.setAttribute("data-id", arr[i].video_id);
                    itemImage.setAttribute("data-type", rightContent.CONTENT_TYPE[2]);
                    if (arr[i].source_url && arr[i].source_url.indexOf("www.youtube.com") != -1) {
                        itemImage.setAttribute("data-url", arr[i].source_url);
                    } else {
                        itemImage.setAttribute("data-url", arr[i].media_url);
                    }
                    itemImage.setAttribute("data-date", arr[i].date_publish);
                    var imageTag = document.createElement("img");
                    if (arr[i].image_url) {
                        imageTag.setAttribute("src", arr[i].image_url);
                    } else {
                        imageTag.setAttribute("src", "img/no-image-available.png");
                    }
                    itemImage.appendChild(imageTag);
                    itemImage.appendChild(renderHTML.createOverlayElement(arr[i].title, null,arr[i].channel.title, arr[i].date_publish, "medium"));

                    rowElement.appendChild(itemImage);
                    channelDetail.indexEnd++;
                }
                if (!isAppend) {
                    channelDetail.displayVideoResult();
                }
            }
            namespace.hiddenLoadingVideo();
            namespace.unBlockUI();
        },
        renderVideoCarousel: function (data, sectionId, title, className) {
            var parentElement = document.getElementById(sectionId);
            parentElement.setAttribute("class", "carousel-section");
            parentElement.style.height = "210px";
            var titleElement = document.createElement("h3");
            titleElement.setAttribute("class", "carousel-title");
            titleElement.innerHTML = title;
            parentElement.appendChild(titleElement);
            data = JSON.parse(data);
            if (data && data.videos && data.videos.length > 0) {
            	if(title == "Best of the Web") {
            		window.bestOfWeb = data.videos;
            	}
                var i, dataTmp = data.videos, leng = dataTmp.length;
                var divElement = document.createElement("ul");
                var docfrag = document.createDocumentFragment();
                divElement.setAttribute("class", "carousel-container carousel " + className);
                for (i = 0; i < leng; i++) {
                	if(i < 7) {//draw only 7 doms
                		var carouselItem = document.createElement("li");
                        carouselItem.setAttribute("class", "medium-carousel-item");
                        carouselItem.setAttribute("data-id", dataTmp[i].video_id);
                        carouselItem.setAttribute("data-type", rightContent.CONTENT_TYPE[2]);
                        if (dataTmp[i].source_url && dataTmp[i].source_url.indexOf("www.youtube.com") != -1) {
                            carouselItem.setAttribute("data-url", dataTmp[i].source_url);
                        } else {
                            carouselItem.setAttribute("data-url", dataTmp[i].media_url);
                        }
                        carouselItem.setAttribute("data-date", dataTmp[i].date_publish);
                        var imageElement = document.createElement("img");
                        imageElement.setAttribute("src", dataTmp[i].image_url);
                        carouselItem.appendChild(imageElement);
                        carouselItem.appendChild(renderHTML.createOverlayElement(dataTmp[i].title, null, dataTmp[i].channel.title, dataTmp[i].date_publish, "medium"));
                        docfrag.appendChild(carouselItem);
                	}
                    
                }
                divElement.appendChild(docfrag);
                parentElement.appendChild(divElement);
            }
        },
        renderTrendingTopics: function (data, sectionId, title, className) {
            var parentElement = document.getElementById(sectionId);
            parentElement.setAttribute("class", "carousel-section");
            parentElement.style.height = "210px";
            if (title != "") {
                var titleElement = document.createElement("h3");
                titleElement.setAttribute("class", "carousel-title");
                titleElement.innerHTML = title;
                parentElement.appendChild(titleElement);
            }
            data = JSON.parse(data);
            if (data && data.length > 0) {
                var i, leng = data.length;
                var divElement = document.createElement("ul");
                divElement.setAttribute("class", "carousel-container carousel " + className);
                var docfrag = document.createDocumentFragment();
                for (i = 0; i < leng; i++) {
                    var carouselItem = document.createElement("li");
                    carouselItem.setAttribute("class", "medium-carousel-item");
                    carouselItem.setAttribute("data-id", data[i].channel_id);
                    carouselItem.setAttribute("data-type", rightContent.CONTENT_TYPE[1]);
                    var imageElement = document.createElement("img");
                    imageElement.setAttribute("src", data[i].image_url);
                    imageElement.setAttribute("width", "260");
                    imageElement.setAttribute("height", "145");
                    carouselItem.appendChild(imageElement);
                    carouselItem.appendChild(renderHTML.createOverlayElement(data[i].title, null, null, data[i].date_created, "medium"));
                    docfrag.appendChild(carouselItem);
                }
                divElement.appendChild(docfrag);
                parentElement.appendChild(divElement);
            }
        },
        createOverlayElement: function (title, subtitle, channelTitle, timeAgo, type) {
            var rootElement = document.createElement("div");
            rootElement.setAttribute("class", "item-overlay");

            var contentElement = document.createElement("div");
            contentElement.setAttribute("class", "item-overlay-content");

            var parentTitleContentElement = document.createElement("div");
            parentTitleContentElement.setAttribute("class", type == "big" ? "title-content" : "medium-title-content");

            var mainTitleContentElement = document.createElement("div");
            mainTitleContentElement.setAttribute("class", type == "big" ? "big-title" : "medium-title");
            mainTitleContentElement.innerHTML = title;

            var subTitleContentElement = document.createElement("div");
            subTitleContentElement.setAttribute("class", type == "big" ? "big-sub-title" : "medium-sub-title");
            subTitleContentElement.innerHTML = subtitle == null ? "" : subtitle;
            if (type != "big") {
                var channelElement = document.createElement("span");
                channelElement.setAttribute("class", "channel-title");
                channelElement.innerHTML = channelTitle;
                subTitleContentElement.appendChild(channelElement);
                var publishedElement = document.createElement("span");
                publishedElement.setAttribute("class", "published-time");
                publishedElement.innerHTML = namespace.calculateTimeAgo(timeAgo);
                subTitleContentElement.appendChild(publishedElement);
            }
            parentTitleContentElement.appendChild(mainTitleContentElement);
            parentTitleContentElement.appendChild(subTitleContentElement);
            contentElement.appendChild(parentTitleContentElement);
            rootElement.appendChild(contentElement);


            return rootElement;
        },
        renderRelatedVideoCarouselHTML: function () {
            var videoId = document.getElementById("playingVideoId").value;
            var carouselSelected = rightContent.getSelectedRow().element.querySelector(".carousel-container");
            if (videoId && carouselSelected) {
                var relatedCarousel = relatedVideoSection.querySelector(".carousel-container");
                if (relatedCarousel == null) {
                    relatedVideoSection.innerHTML = "";
                    relatedVideoSection.appendChild(carouselSelected.cloneNode(true));
                } else {
                    var listCarousel = relatedCarousel.children;
                    var selectedElement = relatedVideoSection.querySelector(".selected");
                    if (selectedElement) {
                        namespace.removeClass(selectedElement, "selected");
                    }
                    if (!renderHTML.isContainCarousel(videoId, listCarousel)) {
                        relatedVideoSection.innerHTML = "";
                        relatedVideoSection.appendChild(carouselSelected.cloneNode(true));
                    }
                }
                namespace.setGuideFavourite(namespace.isFavouritedVideo(videoId));
            }
            this.slideHighlightToHeadPlaylist();
        },
        isContainCarousel: function (videoId, carouselElement) {
            var result =  false;
            if (carouselElement.length > 0) {
                for (var i = 0; i < carouselElement.length; i++) {
                    if (carouselElement[i].getAttribute("data-id") == videoId) {
                        namespace.addClass(carouselElement[i], "selected");
                        result = true;
                        break;
                    }
                }
            }
            return result;
        },
        renderRelatedVideoListHTML: function (data, pageSize) {
            var parentElement = document.getElementById("related-video-section");
            parentElement.innerHTML = "";
            parentElement.style.height = "210px";
            if (data && data.length > 0) {
                var i;
                var divElement = document.createElement("ul");
                divElement.setAttribute("class", "carousel-container carousel videos");
                var docfrag = document.createDocumentFragment();
                for (i = 0; i < pageSize; i++) {
                    var carouselItem = document.createElement("li");
                    if (i == 0) {
                        carouselItem.setAttribute("class", "medium-carousel-item selected");
                        namespace.setGuideFavourite(namespace.isFavouritedVideo(data[i].video_id));
                    } else {
                        carouselItem.setAttribute("class", "medium-carousel-item");
                    }
                    carouselItem.setAttribute("data-id", data[i].video_id);
                    carouselItem.setAttribute("data-type", rightContent.CONTENT_TYPE[2]);
                    carouselItem.setAttribute("data-url", data[i].media_url);
                    carouselItem.setAttribute("data-date", data[i].date_publish);
                    var imageElement = document.createElement("img");
                    imageElement.setAttribute("src", data[i].image_url);
                    imageElement.setAttribute("width", "260");
                    imageElement.setAttribute("height", "145");
                    carouselItem.appendChild(imageElement);
                    carouselItem.appendChild(renderHTML.createOverlayElement(data[i].title, null, null, data[i].date_publish, "medium"));
                    docfrag.appendChild(carouselItem);
                }
                divElement.appendChild(docfrag);
                parentElement.appendChild(divElement);
                this.slideHighlightToHeadPlaylist();
            }
        },
        displayChannelDetail: function(data) {
            data = JSON.parse(data);
            detailSection.getElementsByTagName("img")[0].setAttribute("src", data.image_url);
            detailSection.querySelector(".title").innerHTML = data.title;
            detailSection.querySelector(".description").innerHTML = data.description;
            detailSection.setAttribute("data-channel-id", data.channel_id);
            channelDetail.setGuideLikeChannel(namespace.isFavouritedVideo(data.channel_id));
        },
        insertDataToRelatedVideoHTML: function (element, data, append) {
            if (data) {
                var docfrag = document.createDocumentFragment();
                var carouselItem = document.createElement("li");
                carouselItem.setAttribute("class", "medium-carousel-item");
                carouselItem.setAttribute("data-id", data.video_id);
                carouselItem.setAttribute("data-type", "video");
                carouselItem.setAttribute("data-url", data.media_url);
                carouselItem.setAttribute("data-date", data.date_publish);
                var imageElement = document.createElement("img");
                imageElement.setAttribute("src", data.image_url);
                imageElement.setAttribute("width", "260");
                imageElement.setAttribute("height", "145");
                carouselItem.appendChild(imageElement);
                carouselItem.appendChild(renderHTML.createOverlayElement(data.title, null, null, data.date_publish, "medium"));
                docfrag.appendChild(carouselItem);
                if (append) {
                    element.appendChild(docfrag);
                } else {
                    element.insertBefore(docfrag, element.childNodes[0]);
                }
            }
        },
        slideHighlightToHeadPlaylist: function () {
            var selectedElement = relatedVideoSection.querySelector(".selected");
            var leftPosition = namespace.cssProperty(relatedVideoSection.children[0], "left");
            if (leftPosition == "auto") {
                leftPosition = relatedVideoSection.children[0].offsetLeft;
            } else {
                leftPosition = parseInt(leftPosition.substring(0, (leftPosition.length - 2)));
            }
            var position = selectedElement.getBoundingClientRect();
            relatedVideoSection.children[0].style.left = leftPosition - (Math.floor(position.left) - 130) + "px";
        },
        slideHighlightToHeadPlaylistV2: function () {
            relatedVideoSection.children[0].style.left = (window.paginationPlaylist.currentPlayingIndex == 0) ? "90px" :  "-200px";
        },
        markHighlightItem: function (element, index) {
            var listCarousel = element.children;
            namespace.removeClass(element.querySelector(".selected"), 'selected');
            if (listCarousel && index < listCarousel.length) {
                namespace.addClass(listCarousel[index], "selected");
            }
        }
    }
})(window.FREQ);