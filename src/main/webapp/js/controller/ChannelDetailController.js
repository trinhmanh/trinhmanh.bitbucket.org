(function(namespace){
    /*
    * Author: LongLV
    */
    var resultVideo = document.getElementById("list-video-section");
    var rightElement = document.getElementById("right-content");
    var channelGuide = document.getElementById("channel-guide");
    var gridData = new namespace.GridData();
    var detailSection = document.getElementById("channel-detail-section");

    namespace.ChannelDetail = function () {
    };

    namespace.ChannelDetail.prototype = {
        videoList: [],
        indexEnd: 0,
        displayVideoResult: function() {
            document.getElementById("left-navigation").style.display = "none";
            document.querySelector(".left-background").style.display = "none";
            rightElement.style.paddingLeft = "0px";
            document.querySelector(".back-guide").style.display = "none";
            rightElement.querySelector(".first-level").style.display = "none";
            rightElement.querySelector(".second-level").style.display = "block";
            document.getElementById("filter-channel").style.display = "none";
            document.getElementById("detail-channel").style.display = "block";
            detailSection.querySelector(".description").style.width = "99%";
            setTimeout(function () {
                detailSection.querySelector(".description").style.width = "100%";
            }, 500);
            videoGrid.setSelectedRowByIndex(0,0);
        },
        hiddenToFirstLevel: function (element) {
            document.getElementById("left-navigation").style.display = "block";
            document.querySelector(".left-background").style.display = "block";
            resultVideo.innerHTML = "";
            rightElement.style.paddingLeft = "120px";
            rightElement.querySelector(".first-level").style.display = "block";
            rightElement.querySelector(".second-level").style.display = "none";
            document.getElementById("filter-channel").style.display = "none";
            document.getElementById("detail-channel").style.display = "none";
            var position = element.getBoundingClientRect();
            if (position.top < 95 || (position.top + position.height) > 620) {
                element.parentElement.parentElement.scrollIntoView({
                    block: "start",
                    behavior: "smooth"
                });
            }
        },
        getChannelInfo: function () {
            return {
                dataId: detailSection.getAttribute("data-channel-id"),
                type: "channel"
            }
        },
        hiddenToSecondLevel: function () {
            document.getElementById("filter-channel").style.display = "block";
            document.getElementById("detail-channel").style.display = "none";
            document.querySelector(".back-guide").style.display = "block";
        },
        showForm: function(dataId) {
            if(dataId != undefined) {
                namespace.loadingVideo();
                var url = "/channels/"+ dataId;
                freService.getContentByURL(url, renderHTML.displayChannelDetail);
                url = "/channels/"+ dataId + "/videos?page_size=20";
                window.paginationPlaylist.clearData();
                window.paginationPlaylist.pageSize = 20;
                window.paginationPlaylist.setURLRequest("/channels/" + dataId + "/videos");
                window.isPlayFromChannelDetails = true;
                freService.getContentByURL(url, renderHTML.renderVideosList);
            }
        },
        loadMoreVideoList: function () {
            var dataId = detailSection.getAttribute("data-channel-id"),
                pageSize = detailSection.getAttribute("data-page-size"),
                pageOffset = detailSection.getAttribute("data-page-offset"),
                pageMark = detailSection.getAttribute("data-page-mark");
            if (dataId && pageOffset && pageOffset != "null") {
                url = "/channels/"+ dataId + "/videos?page_size=" + pageSize + "&page_offset=" + pageOffset;
                freService.getContentByURL(url, renderHTML.renderVideosList, true);
            }
        },
        setGuideLikeChannel: function (isFavourite) {
            if (isFavourite) {
                channelGuide.querySelector(".button").style.background = "url(img/thumb-down.png)";
                channelGuide.querySelector(".text").innerHTML = "remove channel from";
            } else {
                channelGuide.querySelector(".button").style.background = "url(img/thumb-up.png)";
                channelGuide.querySelector(".text").innerHTML = "save channel to";
            }
            channelGuide.querySelector(".button").style.backgroundRepeat = "no-repeat";
            channelGuide.querySelector(".button").style.backgroundSize = "17px 17px";
            channelGuide.querySelector(".button").style.backgroundPosition = "center";
        },
        generateRelatedVideo: function (selectedItem) {
            var index = selectedItem.data_index;
            document.getElementById("playingVideoId").value = this.videoList[index].video_id;
            //TODO
            //This function is under development in third sprint
            var data = this.videoList.slice(index, index + 8);
            renderHTML.renderRelatedVideoListHTML(data, data.length);
        },
        processKeyEvent: function(keyCode) {
            var currentSelectedRow = videoGrid.getSelectedRow();
            var currentSelectedItem = videoGrid.getSelectedItem();
            var newIndex;
            switch (keyCode) {
                case window.VK_DOWN:
                    newIndex = currentSelectedRow.index + 1;
                    videoGrid.setSelectedRowByIndex(newIndex, currentSelectedItem.index);
                    break;
                case window.VK_ENTER:
                    console.log("Call video in VideoCarousel");
                    //this.generateRelatedVideo(currentSelectedItem);
                    window.paginationPlaylist.currentPlayingIndex = currentSelectedRow.index * 4 + currentSelectedItem.index;
                    window.paginationPlaylist.setSelectedToCurrentPlaying();
                    window.paginationPlaylist.renderPlaylist();
                    renderHTML.markHighlightItem(window.paginationPlaylist.getDOMList(), ((window.paginationPlaylist.currentPlayingIndex == 0) ? 0 : 1));
                    renderHTML.slideHighlightToHeadPlaylistV2();
                    playerTivo.fullScreenViewByVideoDTO(currentSelectedItem.dataObject);
                    break;
                case window.VK_LEFT:
                    newIndex = currentSelectedItem.index - 1;
                    videoGrid.setSelectedItemByIndex(newIndex, currentSelectedRow.index);
                    break;
                case window.VK_RIGHT:
                    newIndex = currentSelectedItem.index + 1;
                    videoGrid.setSelectedItemByIndex(newIndex, currentSelectedRow.index);
                    break;
                case window.VK_UP:
                    newIndex = currentSelectedRow.index - 1;
                    if (newIndex >= 0) {
                        videoGrid.setSelectedRowByIndex(newIndex, currentSelectedItem.index);
                    }
                    break;
            }
        }
    }
})(window.FREQ);