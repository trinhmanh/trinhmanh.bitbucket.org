(function (namespace) {

    /*
     * Author: LongLV
     * CreateDate: 24/08/2016
     * RightContent.js is file for all of function are execute on Left Navigation
     */

    var rightElement = document.getElementById("right-content"),
        firstLevelContent = rightElement.querySelector(".first-level"),
        playingVideoElement = document.getElementById("playingVideoId"),
        playerTivo = new namespace.PlayerTiVo();

    var bestOfWebPointer = 0;

    namespace.RightContent = function () {
    };
    namespace.RightContent.prototype = {
        MENU_STATUS: true,
        isSearchView: false,
        CONTENT_TYPE: ["category", "channel", "video"],
        FIRST_VIDEO: ["channels_promotional"],
        isLastRow: function (index) {
            var listRow = this.getAllRowOfVisibleContent();
            return index > (listRow.length - 1);
        },
        removeClassSelectedRow: function () {
            var listRow = this.getAllRowOfVisibleContent();
            if (listRow && listRow.length > 0) {
                var i, leng = listRow.length;
                for (i = 0; i < leng; i++) {
                    namespace.removeClass(listRow[i], 'active');
                }
            }
        },
        getSelectedRow: function () {
            var listRow = this.getAllRowOfVisibleContent(),
                selectedRow = null;
            if (listRow && listRow.length > 0) {
                var i, leng = listRow.length;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(listRow[i], 'active')) {
                        selectedRow = {
                            index: i,
                            element: listRow[i]
                        };
                        break;
                    }
                }
                if (selectedRow === null) {
                    selectedRow = {
                        index: 0,
                        element: listRow[0]
                    };
                }
            }
            return selectedRow;
        },
        setSelectedRow: function (index, categoryIndex) {
            var prevSelected = this.getSelectedCarouseItem();
            this.removeClassSelectedRow();
            var listRow = this.getAllRowOfVisibleContent();
            if (listRow && index < listRow.length) {
                namespace.addClass(listRow[index], "active");
                var isCategory = listRow[index].querySelector(".category-section");
                if (isCategory && prevSelected) {
                    this.setSelectedCarouseItem(prevSelected.type=="category"?this.getCategoryItemByIndex(categoryIndex).index:0);
                } else {
                    this.setSelectedCarouseItem(this.getSelectedCarouseItem().index);
                }
            }
        },
        getCategoryItemByIndex: function(index) {
            var result = null;
            var allItemInSelectedRow = this.getSelectedListCarousel();
            if (allItemInSelectedRow) {
                while (index >= allItemInSelectedRow.length) {
                    index --;
                }
                result = {
                    index: index,
                    element: allItemInSelectedRow[index]
                };
            }
            return result;
        },
        isLastCarouselItem: function (index, row) {
            return index > (row.length - 1);
        },
        removeClassSelectedCarouselItem: function () {
            var selectedRow = this.getSelectedRow().element;
            namespace.removeClass(selectedRow.querySelector(".selected"), 'selected');
        },
        /**
         * Remove 'selected' class in all items
         */
        removeClassSelectedItems: function (element) {
            namespace.removeClass(element.querySelector(".selected"), 'selected');
        },
        getSelectedListCarousel: function () {
            if (this.getSelectedRow() != null) {
                var selectedRow = this.getSelectedRow().element;
                if (selectedRow.querySelector(".carousel-container")) {
                    return selectedRow.querySelector(".carousel-container").children;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        },
        getSelectedListRelatedCarousel: function () {
            if (document.querySelector("#related-video-section > ul")) {
                return document.querySelector("#related-video-section > ul").children;
            } else {
                return null;
            }
        },
        getSelectedCarouseItem: function () {
            var selectedCarouselItem = null,
                listCarousel = this.getSelectedListCarousel();
            if (listCarousel && listCarousel.length > 0) {
                var i, leng = listCarousel.length;
                var data = null;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(listCarousel[i], 'selected')) {
                        if (listCarousel[i] && listCarousel[i].getAttribute("data-type") == "video") {
                            data = this.convertVideoDTO(listCarousel[i]);
                        }
                        if (listCarousel[i] && listCarousel[i].getAttribute("data-type") == "channel") {
                            data = this.convertChannelDTO(listCarousel[i]);
                        }
                        selectedCarouselItem = {
                            index: i,
                            element: listCarousel[i],
                            type: listCarousel[i].getAttribute("data-type"),
                            dataId: listCarousel[i].getAttribute("data-id"),
                            dataObject: data
                        };
                    }
                }
                if (selectedCarouselItem === null) {
                    if (listCarousel[0] && listCarousel[0].getAttribute("data-type") == "video") {
                        data = this.convertVideoDTO(listCarousel[0]);
                    }
                    if (listCarousel[0] && listCarousel[0].getAttribute("data-type") == "channel") {
                        data = this.convertChannelDTO(listCarousel[0]);
                    }
                    selectedCarouselItem = {
                        index: 0,
                        element: listCarousel[0],
                        type: listCarousel[0].getAttribute("data-type"),
                        dataId: listCarousel[0].getAttribute("data-id"),
                        dataObject: data
                    };
                }
            }
            return selectedCarouselItem;

        },
        convertVideoDTO: function (element) {
            var videoDto = new namespace.VideoDTO();
            videoDto.videoId = element.getAttribute("data-id");
            videoDto.channelId = null;
            videoDto.channelTitle = element.querySelector(".channel-title").innerHTML;
            videoDto.title = element.querySelector(".medium-title").innerHTML;
            videoDto.videoURL = element.getAttribute("data-url");
            videoDto.imgURL = element.querySelector("img").src;
            videoDto.datePublish = element.getAttribute("data-date");
            return videoDto;
        },
        convertChannelDTO: function (element) {
            var channelDTO = new namespace.ChannelDTO();
            channelDTO.channelId = element.getAttribute("data-id");
            var classList = element.parentElement.classList;
            channelDTO.className = classList[classList.length - 1];
            return channelDTO;
        },
        setSelectedCarouseItem: function (index) {
            var listCarousel = this.getSelectedListCarousel();
            this.removeClassSelectedCarouselItem();
            if (listCarousel && index < listCarousel.length) {
                namespace.addClass(listCarousel[index], "selected");
                this.generateRecBorder(listCarousel[index]);
            }
        },
        /* Get selected item in related video carousel.
         * Only use for pause-video */
        getSelectedRelatedCarouselItem: function () {
            var selectedCarouselItem = null;
            var listCarousel = this.getSelectedListRelatedCarousel();
            if (listCarousel && listCarousel.length > 0) {
                var i, length = listCarousel.length;
                for (i = 0; i < length; i++) {
                    if (namespace.hasClass(listCarousel[i], 'selected')) {
                        var data = null;
                        if (listCarousel[i] && listCarousel[i].getAttribute("data-type") == "video") {
                            data = this.convertVideoDTO(listCarousel[i]);
                        }
                        selectedCarouselItem = {
                            index: i,
                            element: listCarousel[i],
                            type: listCarousel[i].getAttribute("data-type"),
                            dataId: listCarousel[i].getAttribute("data-id"),
                            dataObject: data
                        };
                    }
                }
            }
            return selectedCarouselItem;
        },
        /* Mark a item as selected with border.
         * Only use for pause-video */
        setSelectedRelatedCarouseItem: function (index) {
            var listCarousel = document.querySelector('#related-video-section > ul').children;
            this.removeClassSelectedItems(document.querySelector('#related-video-section > ul'));
            if (listCarousel && index < listCarousel.length) {
                namespace.addClass(listCarousel[index], "selected");
                this.generateRecBorder(listCarousel[index]);
                var selectedVideo = this.getSelectedRelatedCarouselItem();
                if (selectedVideo) {
                    namespace.setGuideFavourite(namespace.isFavouritedVideo(selectedVideo.dataId));
                }
            }
        },
        generateRecBorder: function (element) {
            var position = element.getBoundingClientRect();
            if ( (position.top < 95 || (position.top + position.height) > 625) && !this.isSearchView) {
                element.parentElement.parentElement.scrollIntoView({
                    block: "start",
                    behavior: "smooth"
                });
            }
            if (element.getAttribute("data-type") == this.CONTENT_TYPE[0]) {
                return true;
            }
            var leftPosition = namespace.cssProperty(element.parentElement, "left");
            if (leftPosition == "auto") {
                leftPosition = element.parentElement.offsetLeft;
            } else {
                leftPosition = parseInt(leftPosition.substring(0, (leftPosition.length - 2)));
            }
            if (position.right > 1280) {
                element.parentElement.style.left = (leftPosition - position.width - 25) + "px";
            }
            if (position.left < 130) {
                element.parentElement.style.left = (leftPosition + (130 - position.left) + 15) + "px";
            }
            if (position.left > 1280) {
                element.parentElement.style.left = (leftPosition - (position.left - 1280) - position.width - 25) + "px";
            }

        },
        resetPosition: function () {
            var activeElements = firstLevelContent.querySelectorAll(".active");
            var selectedElement = firstLevelContent.querySelectorAll(".selected");
            var carouselElement = firstLevelContent.querySelectorAll(".carousel-container");
            var i;
            if (carouselElement && carouselElement.length > 0) {
                for (i = 0; i < carouselElement.length; i++) {
                    var leftPosition = namespace.cssProperty(carouselElement[i], "left");
                    if (leftPosition.indexOf("px") != -1) {
                        leftPosition = parseInt(leftPosition.substring(0, (leftPosition.length - 2)));
                        if (leftPosition < 0) {
                            carouselElement[i].style.left = "20px";
                        }
                    }
                }
            }
            if (activeElements && activeElements.length > 0) {
                for (i = 0; i < activeElements.length; i++) {
                    namespace.removeClass(activeElements[i], "active");
                }
            }
            if (selectedElement && selectedElement.length > 0) {
                for (i = 0; i < selectedElement.length; i++) {
                    namespace.removeClass(selectedElement[i], "selected");
                }
            }
            rightElement.scrollTop = 0;
        },
        expandContent: function () {
            rightElement.style.paddingLeft = "120px";
        },
        collapseContent: function () {
            rightElement.style.paddingLeft = "270px";
        },
        getAllPagesInRightContent: function () {
            if (firstLevelContent.children && firstLevelContent.children.length > 0) {
                return firstLevelContent.children;
            } else {
                throw "Cannot get all pages in right content";
            }
        },
        getContentVisible: function () {
            var allPageContent = this.getAllPagesInRightContent();
            if (allPageContent) {
                var i, leng = allPageContent.length;
                for (i = 0; i < leng; i++) {
                    if (window.getComputedStyle(allPageContent[i]).display != 'none') {
                        return allPageContent[i];
                    }
                }
            } else {
                console.error("Cannot find visible element!");
                return null;
            }

        },
        getAllRowOfVisibleContent: function () {
            var visiblePage = this.getContentVisible();
            if (visiblePage) {
                var allRow = visiblePage.children;
                var result = [];
                if (allRow) {
                    var i, leng = allRow.length;
                    for (i = 0; i < leng; i++) {
                        if (allRow[i].tagName == 'DIV' && allRow[i].children.length > 0) {
                            result.push(allRow[i]);
                        }
                    }
                }
                return result;
            } else {
                return null;
            }
        },
        isSecondScreen: function () {
            return (window.getComputedStyle(document.querySelector(".second-level"), null).display != "none");
        },
        isChannelFilterPage: function () {
            return (window.getComputedStyle(document.getElementById("filter-channel"), null).display != "none");
        },
        isChannelDetailPage: function () {
            return (window.getComputedStyle(document.getElementById("detail-channel"), null).display != "none");
        },
        isFullScreenView: function () {
            return (window.getComputedStyle(document.getElementById("video-player"), null).display != "none");
        },
        hiddenAllContent: function () {
            try {
                var allPageContent = this.getAllPagesInRightContent();
                var leng = allPageContent.length;
                var i;
                for (i = 0; i < leng; i++) {
                    allPageContent[i].style.display = 'none';
                }
            } catch (e) {
                console.log(e);
            }
        },
        updateContentByLeftMenu: function (tab) {
            this.hiddenAllContent();
            if (tab != undefined) {
                firstLevelContent.querySelector("#" + tab).style.display = 'block';
                this.showSearchView(tab);
            }
        },
        showSearchView: function (tab) {
            if (tab.indexOf("search") != -1) {
                this.isSearchView = true;
                document.getElementById("left-navigation").style.display = "none";
                document.querySelector(".left-background").style.display = "none";
                rightElement.style.paddingLeft = "120px";
                namespace.addClass(document.getElementById("left-navigation"), "menu-min");
                searchForm.clearInputField();
                searchForm.resetActiveToSearchForm(true);
                searchForm.setDefaultSelectedItem();
                this.MENU_STATUS = false;
                document.querySelector(".back-guide").style.display = "block";
            }
        },
        hiddenSearchView: function () {
            this.isSearchView = false;
            this.MENU_STATUS = true;
            document.getElementById("left-navigation").style.display = "block";
            document.querySelector(".left-background").style.display = "block";
            rightElement.style.paddingLeft = "270px";
            namespace.removeClass(document.getElementById("left-navigation"), "menu-min");
            var searchField = firstLevelContent.lastElementChild;
            namespace.removeClass(searchField.firstElementChild, "active");
            leftMenu.setSelectedFirstElement();
        },
        updateContentByChannel: function (selectedCarouselItem) {
            switch (selectedCarouselItem.type) {
                case this.CONTENT_TYPE[0]:
                    channel.showForm(selectedCarouselItem.dataId);
                    break;
                case this.CONTENT_TYPE[1]:
                    var channelObject = selectedCarouselItem.dataObject;
                    if (this.FIRST_VIDEO.indexOf(channelObject.className) != -1) {
                        namespace.markPlayingFirstVideoInChannel();
                        namespace.loadingVideo();
                        window.paginationPlaylist.setURLRequest("/channels/" + channelObject.channelId + "/videos");
                        freService.getContentByURL(window.paginationPlaylist.urlRequest, rightContent.playFirstVideoInChannel);
                    } else {
                        document.getElementById("prevPage").value = "first-level";
                        if (this.isChannelFilterPage()) {
                            document.getElementById("prevPage").value = "second-level";
                        }
                        namespace.markPlayingFromChannelDetails();
                        channelDetail.showForm(channelObject.channelId);
                    }
                    break;
                case this.CONTENT_TYPE[2]:
                    console.log("Call video in RightContent");
                    this.playVideo(selectedCarouselItem);
                    break;
            }
        },
        playVideo: function (selectedCarouselItem) {
            playingVideoElement.value = selectedCarouselItem.dataObject.videoId;
            if (window.isPlayFromChannel || window.isPlayFromChannelDetails) {
                // No feature resume the previous video while playing video from channel
                window.paginationPlaylist.setSelectedToCurrentPlaying();
                window.paginationPlaylist.renderPlaylist();
                renderHTML.markHighlightItem(window.paginationPlaylist.getDOMList(), ((window.paginationPlaylist.currentPlayingIndex == 0) ? 0 : 1));
                renderHTML.slideHighlightToHeadPlaylistV2();
                playerTivo.fullScreenViewByVideoDTO(selectedCarouselItem.dataObject);
            } else {
                if (namespace.isCurrentVideo(selectedCarouselItem.dataId)) {
                    // Play the previous video
                    namespace.log("Resume Previous video: " + selectedCarouselItem.dataId);
                    document.getElementById("video-player").style.display = "block";
                    if (namespace.isCurrentVideoPlaying()) {
                        playerTivo.play();
                    }
                } else {
                    // Play a new video
                    renderHTML.renderRelatedVideoCarouselHTML();
                    playerTivo.fullScreenViewByVideoDTO(selectedCarouselItem.dataObject);
                }
            }
            /*if (namespace.isCurrentVideo(selectedCarouselItem.dataId) && !window.isPlayFromChannel) {
                // Play the previous video
                namespace.log("Resume Previous video: " + selectedCarouselItem.dataId);
                document.getElementById("video-player").style.display = "block";
                if (namespace.isCurrentVideoPlaying()) {
                    playerTivo.play();
                }
            } else {
                // Play a new video
                if (!window.isPlayFromChannel) {
                    renderHTML.renderRelatedVideoCarouselHTML();
                }
                playerTivo.fullScreenViewByVideoDTO(selectedCarouselItem.dataObject);
            }*/
        },
        playFirstVideoInChannel: function (data) {
            data = JSON.parse(data);
            if (data && data.videos && data.videos.length > 0) {
                renderHTML.renderRelatedVideoListHTML(data.videos, window.paginationPlaylist.domSize);
                window.paginationPlaylist.clearData();
                window.paginationPlaylist.appendData(data.videos, data.page.page_offset);
                playingVideoElement.value = data.videos[0].video_id;
                playerTivo.fullScreenViewByVideoDTO(namespace.convertResponseData2VideoDTO(data.videos[0]));
            }
        },
        processLeftKey: function (index) {
            if (this.isSearchView && searchForm.isSearchFormActive()) {
                searchForm.setSelectedItem(false);
                return true;
            }
            var newIndex;
            if (this.isFullScreenView()) {
                if (playerTivo.isPaused() || playerTivo.isEnded() || playerTivo.isPlaying()) {
                    if (window.isPlayFromChannel || window.isPlayFromChannelDetails) {
                        if (playerTivo.isPlaying() && !window.paginationPlaylist.isFirstItemPlaying()) {
                            // Play previous video while playing
                            var item = {
                                type: "video",
                                dataObject: window.paginationPlaylist.getPreviousVideoDTO()
                            };
                            window.paginationPlaylist.currentPlayingIndex--;
                            this.updateContentByChannel(item);
                        } else if (!playerTivo.isPlaying() && !window.paginationPlaylist.isFirstItem()) {
                            // Navigate highlight in playlist
                            window.paginationPlaylist.moveSelectedToLeft();
                            this.setSelectedRelatedCarouseItem(window.paginationPlaylist.getSelectedIndexInWindow());
                        }
                    } else {
                        newIndex = index - 1;
                        if (newIndex >= 0) {
                            this.setSelectedRelatedCarouseItem(newIndex);
                            if (playerTivo.isPlaying()) {
                                this.updateContentByChannel(this.getSelectedRelatedCarouselItem());
                            }
                        }
                    }
                }
            } else if (!this.isSecondScreen()) {
                newIndex = index - 1;
                if(bestOfWebPointer > 0) {
                    bestOfWebPointer -= 1;
                }
                var carousels = this.getSelectedListCarousel();
                if (newIndex > 0 || bestOfWebPointer == 0) {
                    this.setSelectedCarouseItem(newIndex);
                } else if(newIndex == 0) {
                    for(var i = 0; i < 7; i++) {
                        var selectedIndex = bestOfWebPointer + i - 1;
                        if(selectedIndex < 0 || selectedIndex >= window.bestOfWeb.length) {
                            return;
                        }
                        var item = window.bestOfWeb[selectedIndex];
                        var carousel = carousels[i];
                        carousel.querySelector(".medium-title").innerHTML = item.title;
                        carousel.childNodes[0].src = item.image_url;
                        // img.src="jkjfa.com";
                    }
                } else if (!this.isSearchView) {
                    leftMenu.expandMenu();
                    this.collapseContent();
                    this.MENU_STATUS = true;
                    this.resetPosition();
                }
            } else {
                if (this.isChannelFilterPage()) {
                    channel.processKeyEvent(window.VK_LEFT);
                }
                if (this.isChannelDetailPage()) {
                    channelDetail.processKeyEvent(window.VK_LEFT);
                }
            }
        },
        /* Handle push right key.
         *  If push right key while pause-video status, mark related video on carousel as  */
        processRightKey: function (rowIndex, itemIndex) {
            if (this.isSearchView && searchForm.isSearchFormActive()) {
                searchForm.setSelectedItem(true);
                return true;
            }
            if (this.isFullScreenView()) {
                // Process in player mode
                if (playerTivo.isPaused() || playerTivo.isEnded() || playerTivo.isPlaying()) {
                    if (window.isPlayFromChannel || window.isPlayFromChannelDetails) {
                        if (playerTivo.isPlaying() && !window.paginationPlaylist.isLastItemPlaying()) {
                            // Play next video in playlist while playing
                            var item = {
                                type: "video",
                                dataObject: window.paginationPlaylist.getNextVideoDTO()
                            };
                            window.paginationPlaylist.currentPlayingIndex++;
                            this.updateContentByChannel(item);
                        } else if (!playerTivo.isPlaying() && !window.paginationPlaylist.isLastItem()) {
                            // Navigate highlight in playlist
                            window.paginationPlaylist.moveSelectedToRight();
                            this.setSelectedRelatedCarouseItem(window.paginationPlaylist.getSelectedIndexInWindow());
                        }
                    } else {
                        // TODO: Need to implement for other areas except Play first video in channel
                        var newIndex = itemIndex + 1;
                        if (!this.isLastCarouselItem(newIndex, this.getSelectedListRelatedCarousel())) {
                            this.setSelectedRelatedCarouseItem(newIndex);
                            if (playerTivo.isPlaying()) {
                                this.updateContentByChannel(this.getSelectedRelatedCarouselItem());
                            }
                        }
                    }
                }
            } else if (!this.isSecondScreen()) {
                if (this.MENU_STATUS) {
                    this.setSelectedRow(rowIndex);
                    this.MENU_STATUS = false;
                } else {
                    if(bestOfWebPointer == window.bestOfWeb.length - 1) {
                        return;
                    }
                    bestOfWebPointer += 1;
                    var newIndex = itemIndex + 1;
                    var carousels = this.getSelectedListCarousel();
                    if (!this.isLastCarouselItem(newIndex, this.getSelectedListCarousel())) {
                        if(newIndex == 5) {//Pinter
                            for(var i = 0; i < 7; i++) {
                                var selectedIndex = bestOfWebPointer + (i - newIndex) + 1;
                                if(selectedIndex >= window.bestOfWeb.length) {
                                	return;
                                }
                            	var item = window.bestOfWeb[selectedIndex];
                                var carousel = carousels[i];
                                carousel.querySelector(".medium-title").innerHTML = item.title;
                                carousel.childNodes[0].src = item.image_url;
                                // img.src="jkjfa.com";
                            }
                        } else {
                            this.setSelectedCarouseItem(newIndex);
                        }
                    } else {
                        var newRowIndex = rowIndex + 1;
                        var selectedRow = this.getSelectedRow().element;
                        if (!this.isLastRow(newRowIndex) && selectedRow.querySelector(".category-section")) {
                            this.setSelectedRow(newRowIndex, 0);
                        }
                    }
                }
            } else {
                if (this.isChannelFilterPage()) {
                    channel.processKeyEvent(window.VK_RIGHT);
                }
                if (this.isChannelDetailPage()) {
                    channelDetail.processKeyEvent(window.VK_RIGHT);
                }
            }

        },
        processUpKey: function (index, itemIndex) {
            if (this.isSearchView && searchForm.isSearchFormActive()) {
                if (index == 1 && itemIndex != null){
                    searchForm.setDefaultSelectedItem();
                } else {
                    searchForm.setSelectedRow(false);
                }
                return true;
            }
            if (this.isFullScreenView()) {
                if (playerTivo.isPlaying()) {
                    playerTivo.pause();
                }
                return true;
            }
            if (!this.isSecondScreen()) {
                var newIndex = index - 1;
                if (newIndex >= 0) {
                    this.setSelectedRow(newIndex, itemIndex);
                }
            } else {
                if (this.isChannelFilterPage()) {
                    channel.processKeyEvent(window.VK_UP);
                }
                if (this.isChannelDetailPage()) {
                    channelDetail.processKeyEvent(window.VK_UP);
                }
            }

        },
        processDownKey: function (index, itemIndex) {
            if (this.isSearchView && searchForm.isSearchFormActive()) {
                searchForm.setSelectedRow(true);
                return true;
            }
            if (this.isFullScreenView()) {
                if (playerTivo.isPaused()) {
                    playerTivo.play();
                }
                return true;
            }
            if (!this.isSecondScreen()) {
                var newIndex = index + 1;
                if (!this.isLastRow(newIndex)) {
                    this.setSelectedRow(newIndex, itemIndex);
                }
            } else {
                if (this.isChannelFilterPage()) {
                    channel.processKeyEvent(window.VK_DOWN);
                }
                if (this.isChannelDetailPage()) {
                    channelDetail.processKeyEvent(window.VK_DOWN);
                }
            }
        },
        processEnterKey: function (selectedCarouselItem) {
            if (this.isSearchView && searchForm.isSearchFormActive()) {
                searchForm.processEnterEvent();
                return true;
            }
            if (this.isFullScreenView()) {
                // Press enter in play-video mode
                if (playerTivo.isPlaying()) {
                    playerTivo.pause();
                } else {
                    // Press enter while selecting on playlist
                    window.paginationPlaylist.currentPlayingIndex = window.paginationPlaylist.selectedIndex;
                    this.playVideo(selectedCarouselItem);
                }
            } else if (!this.isSecondScreen()) {
                // Press enter at the main page
                this.updateContentByChannel(selectedCarouselItem);
            } else if (this.isChannelFilterPage()) {
                // Press enter in Channel Filter page
                channel.processKeyEvent(window.VK_ENTER);
            } else if (this.isChannelDetailPage()) {
                // Press enter in Channel Detail page
                channelDetail.processKeyEvent(window.VK_ENTER);
            }
        },
        processBackKey: function (index) {
            namespace.hiddenLoadingVideo();
            document.querySelector(".back-guide").style.display = "none";
            currentXHR.abort();
            if (this.isSearchView) {
                if (this.isFullScreenView()) {
                    window.paginationPlaylist.clearData();
                    playerTivo.exitFullScreen();
                    playingVideoElement.value = "";
                } else {
                    if (this.isChannelDetailPage()) {
                        if (document.getElementById("prevPage").value == "first-level") {
                            rightElement.style.paddingLeft = "120px";
                            rightElement.querySelector(".first-level").style.display = "block";
                            rightElement.querySelector(".second-level").style.display = "none";
                            document.getElementById("filter-channel").style.display = "none";
                            document.getElementById("detail-channel").style.display = "none";
                        }
                    } else {
                        this.hiddenSearchView();
                    }
                }
                return true;
            }
            if (window.dialog.isShow()) {
                window.dialog.closeDialog();
                return true;
            }
            if (this.isFullScreenView()) {
                if (window.isPlayFromChannel) {
                    window.paginationPlaylist.clearData();
                }
                playerTivo.exitFullScreen();
                playingVideoElement.value = "";
                return true;
            }
            if (this.isSecondScreen()) {
                if (this.isChannelFilterPage()) {
                    channel.processKeyEvent(window.VK_BACK);
                    rightContent.setSelectedCarouseItem(index);
                }
                if (this.isChannelDetailPage()) {
                    if (document.getElementById("prevPage").value == "first-level") {
                        var selectedElement = rightContent.getSelectedCarouseItem().element;
                        channelDetail.hiddenToFirstLevel(selectedElement);
                    } else {
                        channelDetail.hiddenToSecondLevel();
                    }
                }
                return true;
            } else {
                dialog.confirmDialog("Exit Frequency", "Are you sure you want to exit Frequency?", "Exit", function () {
                    window.close();
                });
            }
        },
        processPlayKey: function (selectedCarouselItem) {
            if (this.isFullScreenView() && selectedCarouselItem) {
                var videoId = playingVideoElement.value;
                if (playerTivo.isPaused() || playerTivo.isEnded()) {
                    if (videoId == selectedCarouselItem.dataId) {
                        playerTivo.play();
                    } else {
                        playingVideoElement.value = selectedCarouselItem.dataObject.videoId;
                        playerTivo.fullScreenViewByVideoDTO(selectedCarouselItem.dataObject);
                    }
                }
            }
        },
        processPauseKey: function () {
            if (this.isFullScreenView()) {
                if (playerTivo.isPaused()) {
                    playerTivo.play();
                } else {
                    playerTivo.pause();
                }
            }
        },
        processForwardKey: function () {
            if (this.isFullScreenView()) {
                playerTivo.forward();
            }
        },
        processBackwardKey: function () {
            if (this.isFullScreenView()) {
                playerTivo.backward();
            }
        },
        processLikeContent: function (selectedItem) {
            if (this.isFullScreenView()) {
                if (playerTivo.isPaused() || playerTivo.isEnded()) {
                    if (selectedItem && !namespace.isFavouritedVideo(selectedItem.dataId)) {
                        if (selectedItem.type == "video" || selectedItem.type == "channel") {
                            favoriteList.push(selectedItem.dataId);
                            namespace.notify("You have added " + selectedItem.type + " " + selectedItem.dataId + " to Favorites List");
                            namespace.setGuideFavourite(true);
                        }
                    }
                }
            } else {
                if (this.isChannelDetailPage()) {
                    selectedItem = channelDetail.getChannelInfo();
                }
                if (selectedItem && !namespace.isFavouritedVideo(selectedItem.dataId)) {
                    if (selectedItem.type == "video" || selectedItem.type == "channel") {
                        favoriteList.push(selectedItem.dataId);
                        namespace.notify("You have added " + selectedItem.type + " " + selectedItem.dataId + " to Favorites List");
                        if (this.isChannelDetailPage()) {
                            channelDetail.setGuideLikeChannel(true);
                        } else {
                            namespace.setGuideFavourite(true);
                        }
                    }
                }
            }
        },
        processUnLikeContent: function (selectedItem) {
            if (this.isFullScreenView()) {
                if (playerTivo.isPaused() || playerTivo.isEnded()) {
                    if (selectedItem && namespace.isFavouritedVideo(selectedItem.dataId)) {
                        if (selectedItem.type == "video" || selectedItem.type == "channel") {
                            var index = favoriteList.indexOf(selectedItem.dataId);
                            favoriteList.splice(index, 1);
                            namespace.notify("You have removed " + selectedItem.type + " " + selectedItem.dataId + " from Favorites List");
                            namespace.setGuideFavourite(false);
                        }
                    }
                }
            } else {
                if (this.isChannelDetailPage()) {
                    selectedItem = channelDetail.getChannelInfo();
                }
                if (selectedItem && namespace.isFavouritedVideo(selectedItem.dataId)) {
                    if (selectedItem.type == "video" || selectedItem.type == "channel") {
                        var index = favoriteList.indexOf(selectedItem.dataId);
                        favoriteList.splice(index, 1);
                        namespace.notify("You have removed " + selectedItem.type + " " + selectedItem.dataId + " from Favorites List");
                        if (this.isChannelDetailPage()) {
                            channelDetail.setGuideLikeChannel(false);
                        } else {
                            namespace.setGuideFavourite(false);
                        }
                    }
                }
            }
        },
        processKeyBoardEvent: function (keyCode) {
            if (dialog.isShow()) {
                dialog.closeDialog();
                return true;
            }

            // Comment because library's issue: cannot change state after buffering for youtube source
            /*if (this.isFullScreenView() && (playerTivo.isBuffering() || playerTivo.isSeeking())) {
                // Prevent from any action while buffering or seeking
                return true;
            }*/

            var rowSelected, selectedCarouselItem;
            if (this.isFullScreenView()) {
                rowSelected = {index: -1};
                selectedCarouselItem = this.getSelectedRelatedCarouselItem();
            } else {
                rowSelected = this.getSelectedRow();
                selectedCarouselItem = this.getSelectedCarouseItem();
            }
            var itemIndex = selectedCarouselItem!=null?selectedCarouselItem.index:null;
            switch (keyCode) {
                case window.VK_UP:
                    if (this.isSearchView && rowSelected.index == 1) {
                        searchForm.resetActiveToSearchForm();
                    }
                    this.processUpKey(rowSelected.index, itemIndex);
                    break;
                case window.VK_DOWN:
                    this.processDownKey(rowSelected.index, itemIndex);
                    break;
                case window.VK_LEFT:
                    if (selectedCarouselItem) {
                        this.processLeftKey(selectedCarouselItem.index);
                    } else {
                        this.processLeftKey(0);
                    }

                    break;
                case window.VK_RIGHT:
                    if (rowSelected != null && selectedCarouselItem != null) {
                        if (leftMenu.isActive()) {
                            leftMenu.collapseMenu();
                            this.expandContent();
                        }
                        this.processRightKey(rowSelected.index, selectedCarouselItem.index);
                    } else {
                        this.processRightKey();
                    }
                    break;
                case window.VK_ENTER:
                    if (this.isFullScreenView()) {
                        selectedCarouselItem = this.getSelectedRelatedCarouselItem();
                    } else {
                        // TODO: Temporary determine Play video Best of web
                        if (rowSelected.index == 1) namespace.markPlayingFromMainPage();
                    }
                    this.processEnterKey(selectedCarouselItem);
                    break;
                case window.VK_BACK:
                    if (selectedCarouselItem) {
                        this.processBackKey(selectedCarouselItem.index);
                    } else {
                        this.processBackKey();
                    }
                    break;
                case window.VK_PLAY:
                    this.processPlayKey(selectedCarouselItem);
                    break;
                case window.VK_PAUSE:
                    this.processPauseKey();
                    break;
                case window.VK_FORWARD:
                    this.processForwardKey();
                    break;
                case window.VK_BACKWARD:
                    this.processBackwardKey();
                    break;
                case window.VK_LIKE:
                    this.processLikeContent(selectedCarouselItem);
                    break;
                case window.VK_UNLIKE:
                    this.processUnLikeContent(selectedCarouselItem);
                    break;
            }
        }
    }
    namespace.SearchView = function () {
    };
    namespace.SearchView.prototype = {
        removeSelected: function () {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            namespace.removeClass(searchField.querySelector(".selected"), "selected");
        },
        isSearchFormActive: function () {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            return namespace.hasClass(searchField, "active");
        },
        setSelectedRow: function (direction) {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            var searchOption = searchField.lastElementChild;
            var selectedRow = searchOption.querySelector(".row-selected");
            var selectedItem = selectedRow.querySelector(".selected");
            if (selectedRow) {
                if (direction) {
                    if (selectedRow.nextElementSibling && !this.isLastPosition(selectedRow)) {
                        if (selectedItem) {
                            namespace.removeClass(selectedItem, "selected");
                        }
                        namespace.removeClass(selectedRow, "row-selected");
                        namespace.addClass(selectedRow.nextElementSibling, "row-selected");
                        namespace.addClass(selectedRow.nextElementSibling.firstElementChild, "selected");
                    } else {
                        if (this.isLastPosition(selectedRow)) {
                            this.removeSelected();
                            namespace.removeClass(searchField, "active");
                            if (searchField.nextElementSibling) {
                                if (searchField.nextElementSibling.querySelector(".selected")) {
                                    namespace.addClass(searchField.nextElementSibling, "active");
                                } else {
                                    rightContent.setSelectedRow(1);
                                }
                            }
                        }
                    }
                } else {
                    if (selectedRow.previousElementSibling) {
                        if (selectedItem) {
                            namespace.removeClass(selectedItem, "selected");
                        }
                        namespace.removeClass(selectedRow, "row-selected");
                        namespace.addClass(selectedRow.previousElementSibling, "row-selected");
                        if (selectedRow.previousElementSibling == searchOption.firstElementChild) {
                            namespace.addClass(selectedRow.previousElementSibling.firstElementChild.nextElementSibling, "selected");
                        } else {
                            namespace.addClass(selectedRow.previousElementSibling.firstElementChild, "selected");
                        }
                    }
                }
            }
        },
        setDefaultSelectedItem: function () {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            var searchOption = searchField.lastElementChild;
            var selectedRow = searchOption.firstElementChild;
            namespace.addClass(selectedRow, "row-selected");
            namespace.addClass(selectedRow.firstElementChild.nextElementSibling, "selected");

        },
        setSelectedItem: function (direction) {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            var searchOption = searchField.lastElementChild;
            var selectedRow = searchOption.querySelector(".row-selected");
            var selectedItem = selectedRow.querySelector(".selected");
            if (selectedItem) {
                if (direction) {
                    if (selectedItem.nextElementSibling) {
                        namespace.removeClass(selectedItem, "selected");
                        namespace.addClass(selectedItem.nextElementSibling, "selected");
                    } else {
                        namespace.removeClass(selectedItem, "selected");
                        namespace.addClass(selectedRow.firstElementChild, "selected");
                    }
                } else {
                    if (selectedItem.previousElementSibling) {
                        namespace.removeClass(selectedItem, "selected");
                        namespace.addClass(selectedItem.previousElementSibling, "selected");
                    } else {
                        namespace.removeClass(selectedItem, "selected");
                        namespace.addClass(selectedRow.lastElementChild, "selected");
                    }
                }
            } else {

            }
        },
        isLastPosition: function (element) {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            var optionSearch = searchField.lastElementChild;
            return element == optionSearch.lastElementChild;
        },
        getKeyValue: function () {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            var optionSearch = searchField.lastElementChild;
            return optionSearch.querySelector(".selected").getAttribute("key-value");
        },
        processEnterEvent: function () {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            var textField = searchField.firstElementChild.querySelector("input");
            var keyValue = this.getKeyValue();
            switch (keyValue) {
                case "delete":
                    var str = textField.value;
                    textField.value = str.substring(0, str.length - 1);
                    break;
                case "search":
                    dialog.showDialog("Sorry, this section is under development!");
                    break;
                default:
                    textField.value += keyValue;
                    break;
            }
        },
        resetActiveToSearchForm: function (reset) {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            namespace.removeClass(searchPage.querySelector(".active"), "active");
            namespace.addClass(searchField, "active");
            if (reset && searchField.nextElementSibling) {
                namespace.removeClass(searchPage.querySelector(".selected"), "selected");
                searchField.nextElementSibling.querySelector(".carousel-container").style.left = "20px";
                searchField.nextElementSibling.nextElementSibling.querySelector(".carousel-container").style.left = "20px";
            }
        },
        clearInputField: function () {
            var searchPage = firstLevelContent.lastElementChild;
            var searchField = searchPage.firstElementChild;
            var textField = searchField.firstElementChild.querySelector("input");
            textField.value = "";
        }
    };

})(window.FREQ);