(function(namespace){
    
    namespace.FrequencyAPI = function() {};
    namespace.FrequencyAPI.prototype = {
        FREQ_PROD_URL: "",
        contructor: function() {
            this.FREQ_PROD_URL = document.getElementById("baseUrl").value;
        },
        updateRefreshAuthenticateInformation: function (data) {
            console.log(JSON.parse(data));
            data = JSON.parse(data);
            if (data.token) {
                document.getElementById("authToken").value = data.token.token_access;
                document.getElementById("authTokenRefresh").value = data.token.token_refresh;
            }
        },
        updateAuthenticateInformation: function (data) {
            console.log(JSON.parse(data));
            data = JSON.parse(data);
            document.getElementById("deviceId").value = data.device_id;
            document.getElementById("authToken").value = data.token.token_access;
            document.getElementById("authTokenRefresh").value = data.token.token_refresh;
        },
        processAuthData: function(data) {
            var _this = new namespace.FrequencyAPI();
            _this.updateAuthenticateInformation(data);
            _this.refreshToken();
            _this.getLayoutPage();
        },
        generateHeaderAjaxCall: function() {
            var headers = [];
            var deviceId = {
                header: "X-Frequency-DeviceId",
                value: document.getElementById("deviceId").value
            },
            eToken = {
                header: "X-Frequency-Auth",
                value: document.getElementById("authToken").value
            };
            headers.push(deviceId);
            headers.push(eToken);
            return headers;
        },
        refreshToken: function () {
            setInterval(function () {
                namespace.log("renew token access to frequency API");
                var _this = new namespace.FrequencyAPI();
                _this.contructor();
                var data = {
                    "token_refresh": document.getElementById("authTokenRefresh").value
                };
                try {
                    namespace.fetchData("POST", _this.generateHeaderAjaxCall(), _this.FREQ_PROD_URL + "/auth/refresh", JSON.stringify(data), _this.updateRefreshAuthenticateInformation);
                } catch(e) {
                    console.error(e);
                }
            }, 600000);
            //2m: 120000
            //14m: 840000
        },
        authenticateFrequency: function() {
            try {
                namespace.fetchData("GET", null, "frequency/auth/device", null, this.processAuthData);
            } catch(e) {
                console.error(e);
            }
        },
        getLayoutPage: function() {
            try {
                namespace.blockUI();
                namespace.fetchData("GET", null, "frequency/app/json", null, this.printLayoutContent);
            } catch(e) {
                namespace.unBlockUI();
            }
        },
        printLayoutContent: function(data) {
            data = JSON.parse(data);
            document.getElementById("baseUrl").setAttribute("value", data.baseUrl);
            renderHTML.renderLeftMenuHtml(data.pages);
            renderHTML.renderRightContentHTML(data.pages);
            namespace.unBlockUI();
        },
        getContentByURL: function(url, func, ...args) {
            this.contructor();
            try {
                url = this.FREQ_PROD_URL + url;
                if (typeof func === "function") {
                    namespace.fetchDataWithArgs("GET", this.generateHeaderAjaxCall(), url, null, func, ...args);
                } else {
                    console.log("Method in parameter is not a function!");
                    namespace.unBlockUI();
                }
            } catch(e) {
                namespace.unBlockUI();
            }
        }
    };
    
    window.addEventListener("load", initApplication, true);
    
    function initApplication() {
        var frequencyApi = new namespace.FrequencyAPI();
        frequencyApi.authenticateFrequency();
    }
    
})(window.FREQ);