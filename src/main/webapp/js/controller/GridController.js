(function(namespace){

    namespace.GridData = function(id) {
        this.resultVideo = document.getElementById(id);
    };
    namespace.GridData.prototype = {
        removeActiveRows: function() {
            namespace.removeClass(this.resultVideo.querySelector(".active"), "active");
        },
        setSelectedRow: function(element, itemIndex){
            if (element) {
                this.removeActiveRows();
                namespace.addClass(element, "active");
                var selectedItem = this.getItemByIndex(itemIndex);
                this.setSelectedItem(selectedItem.element);
            }
        },
        setSelectedRowByIndex: function(index, itemIndex) {
            var dataRows = this.getAllRows(),
                leng = dataRows.length;
            if (index < leng) {
                if(itemIndex == undefined) {
                    var numberOfItem = dataRows[0].children.length
                    itemIndex = numberOfItem - 1;
                }
                this.setSelectedRow(dataRows[index], itemIndex);
            }
            var belt = index + 2;
            if (belt == (leng - 1) && rightContent.isChannelDetailPage()) {
                channelDetail.loadMoreVideoList();
            }

            if (belt == leng && rightContent.isChannelFilterPage()) {
                channel.loadMoreChannel();
            }
        },
        getSelectedRow: function() {
            var result = null;
            var dataRows = this.getAllRows();
            if (dataRows && dataRows.length > 0) {
                var i, leng = dataRows.length;
                for (i = 0; i < leng; i++) {
                    if (namespace.hasClass(dataRows[i], "active")) {
                        result =  {
                            index: i,
                            element: dataRows[i]
                        };
                        break;
                    }
                }
                if (!result) {
                    result =  {
                        index: 0,
                        element: dataRows[0]
                    };
                }
            }
            return result;
        },
        removeSelectedItems: function() {
            var selectedRow = this.resultVideo.querySelector(".active");
            if (selectedRow) {
                namespace.removeClass(selectedRow.querySelector(".selected"), "selected");
            }
        },
        setSelectedItem: function(element) {
            if (element) {
                this.removeSelectedItems();
                namespace.addClass(element, "selected");
                this.generateRecBorder(element);
            }
        },
        setSelectedItemByIndex: function(index, rowIndex) {
            var allItems = this.getAllItemsInSelectedRow(),
                leng = allItems.length;
            if (index < leng && index >= 0) {
                this.setSelectedItem(allItems[index]);
            } else {
                if(index < 0) {
                    this.setSelectedRowByIndex(rowIndex - 1);
                }
                if (index == leng) {
                    this.setSelectedRowByIndex(rowIndex + 1, 0);
                }
            }
        },
        getSelectedItem: function() {
            var selectedRow = this.resultVideo.querySelector(".active");
            if (selectedRow) {
                var selectedItem = selectedRow.querySelector(".selected");
                var allItemInSelectedRow = this.getAllItemsInSelectedRow();
                var i, index = 0;
                for (i = 0; i < allItemInSelectedRow.length; i++) {
                    if (selectedItem == allItemInSelectedRow[i]) {
                        index = i;
                        break;
                    }
                }
                return {
                    index: index,
                    element: selectedItem,
                    data_index: selectedItem.getAttribute("data-index"),
                    type: selectedItem.getAttribute("data-type"),
                    dataId: selectedItem.getAttribute("data-id"),
                    dataObject: selectedItem.getAttribute("data-type")==rightContent.CONTENT_TYPE[2]?rightContent.convertVideoDTO(selectedItem):rightContent.convertChannelDTO(selectedItem)
                };
            } else {
                return null;
            }

        },
        getItemByIndex: function(index) {
            var result = null;
            var allItemInSelectedRow = this.getAllItemsInSelectedRow();
            if (allItemInSelectedRow) {
                while (index >= allItemInSelectedRow.length) {
                    index --;
                }
                result = {
                    index: index,
                    element: allItemInSelectedRow[index]
                };
            }
            return result;
        },
        getAllRows: function() {
            return this.resultVideo.children;
        },
        getAllItemsInSelectedRow: function() {
            var selectedRow = this.getSelectedRow();
            if (selectedRow) {
                return selectedRow.element.children;
            } else {
                return undefined;
            }
        },
        generateRecBorder: function (element) {
            var position = element.getBoundingClientRect();
            if (position.top < 200 || (position.top + position.height) > 650) {
                element.parentElement.scrollIntoView({
                    block: "start",
                    behavior: "smooth"
                });
            }
        }
    }
})(window.FREQ);