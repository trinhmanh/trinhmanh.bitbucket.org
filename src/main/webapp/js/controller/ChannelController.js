(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    
    var resultChannel = document.getElementById("list-channel-section"),
        rightElement = document.getElementById("right-content"),
        channelSection = document.getElementById("channel-page"),
        filterSection = document.getElementById("filter-channel");

    namespace.ChannelFilter = function (){};

    namespace.ChannelFilter.prototype = {
        filterType: ["category-filter", "channel-sort"],
        setSelectedFilter: function (className) {
            namespace.removeClass(filterSection.querySelector(".selected"), "selected");
            namespace.addClass(filterSection.querySelector("." + className), "selected");
        },
        exitFilterSection: function () {
            var dropdown = filterSection.querySelector(".dropdown-group");
            namespace.removeClass(dropdown, "active");
            namespace.removeClass(dropdown.querySelector(".selected"), "selected");
            filterSection.querySelector(".filter-option").style.display = "none";
        },
        getSelectedOption: function () {
            return filterSection.querySelector(".selected-option");
        },
        moveSelectedOption: function (nextDirection) {
            var selectedOption = this.getSelectedOption();
            if (nextDirection) {
                if (selectedOption.nextElementSibling) {
                    namespace.removeClass(selectedOption, "selected-option");
                    namespace.addClass(selectedOption.nextElementSibling, "selected-option");
                }
            } else {
                if (selectedOption.previousElementSibling) {
                    namespace.removeClass(selectedOption, "selected-option");
                    namespace.addClass(selectedOption.previousElementSibling, "selected-option");
                }
            }
        },
        setSelectedOptionDropdown: function () {
            var selectedFilter = filterSection.querySelector(".selected");
            var selectedOption = this.getSelectedOption();
            if (selectedFilter.value != selectedOption.getAttribute("value")) {
                selectedFilter.value = selectedOption.getAttribute("value");
                channel.filterState = false;
                this.exitFilterSection();
                this.recallData();
            }
            filterSection.querySelector(".filter-option").style.display = "none";
        },
        recallData: function () {
            var categoryFilter = filterSection.querySelector(".category-filter");
            var sortFilter = filterSection.querySelector(".channel-sort");
            resultChannel.setAttribute("data-category-id", categoryFilter.value);
            var url = "/categories/"+ categoryFilter.value + "/channels?sort=" + sortFilter.value;
            freService.getContentByURL(url, renderHTML.renderChannelList);
        },
        renderOption: function () {
            var dropdown = filterSection.querySelector(".dropdown-group"),
            filterOption = filterSection.querySelector(".filter-option"),
            element = dropdown.querySelector(".selected"),
            listOption = element.querySelectorAll("option");
            if (listOption && listOption.length > 0) {
                var i, leng = listOption.length;
                filterOption.querySelector("ul").innerHTML = "";
                var documentFrag = document.createDocumentFragment();
                for (i = 0; i < leng; i++) {
                    var liElement = document.createElement("li");
                    liElement.innerHTML = listOption[i].innerHTML;
                    liElement.setAttribute("value", listOption[i].value);
                    if (element.value == listOption[i].value) {
                        liElement.setAttribute("class", "selected-option");
                    }
                    documentFrag.appendChild(liElement);
                }
                filterOption.querySelector("ul").appendChild(documentFrag);
                var position = element.getBoundingClientRect();
                filterOption.style.left = position.left + "px";
                filterOption.style.top = position.height + 25 + "px";
                filterOption.style.width = position.width + "px";
                filterOption.style.display = "block";
            }
        },
        isShowOption: function () {
            return (window.getComputedStyle(filterSection.querySelector(".filter-option"), null).display != "none");
        },
        processKeyboard: function (keyCode) {
            switch (keyCode) {
                case window.VK_DOWN:
                    if (this.isShowOption()) {
                        this.moveSelectedOption(true);
                    } else if(channelGrid.getAllRows().length > 0) {
                        channel.filterState = false;
                        this.exitFilterSection();
                        channelGrid.setSelectedRowByIndex(0, 0);
                    }
                    break;
                case window.VK_ENTER:
                    if (this.isShowOption()) {
                        this.setSelectedOptionDropdown();
                    } else {
                        this.renderOption();
                    }
                    break;
                case window.VK_LEFT:
                    if (!this.isShowOption()) {
                        this.setSelectedFilter(this.filterType[0]);
                    }
                    break;
                case window.VK_RIGHT:
                    if (!this.isShowOption()) {
                        this.setSelectedFilter(this.filterType[1]);
                    }
                    break;
                case window.VK_UP:
                    if (this.isShowOption()) {
                        this.moveSelectedOption(false);
                    }
                    break;
            }
        }
    };
    
    namespace.Channel = function() {};
    namespace.Channel.prototype = {
        filterState: false,
        displayChannelResult: function(data) {
            document.getElementById("left-navigation").style.display = "none";
            document.querySelector(".left-background").style.display = "none";
            rightElement.style.paddingLeft = "0px";
            document.querySelector(".back-guide").style.display = "block";
            rightElement.querySelector(".first-level").style.display = "none";
            rightElement.querySelector(".second-level").style.display = "block";
            document.querySelector(".dropdown-group").scrollIntoView();
            document.getElementById("detail-channel").style.display = "none";
            document.getElementById("filter-channel").style.display = "block";
            channelGrid.setSelectedRowByIndex(0, 0);
        },
        showForm: function(dataId) {
            if (dataId != undefined) {
                namespace.loadingVideo();
                resultChannel.setAttribute("data-category-id", dataId);
                filterSection.querySelector(".category-filter").value = dataId;
                filterSection.querySelector(".channel-sort").value = "alpha";
                var url = "/categories/"+ dataId + "/channels?sort=alpha";
                freService.getContentByURL(url, renderHTML.renderChannelList);
            }
            
        },
        loadMoreChannel: function () {
            var dataId = resultChannel.getAttribute("data-category-id"),
                pageSize = resultChannel.getAttribute("data-page-size"),
                pageOffset = resultChannel.getAttribute("data-page-offset"),
                pageMark = resultChannel.getAttribute("data-page-mark");
            if (dataId && pageOffset && pageOffset != "null") {
                url = "/categories/"+ dataId + "/channels?sort=popular&page_size=" + pageSize + "&page_offset=" + pageOffset;
                freService.getContentByURL(url, renderHTML.renderVideosList, true);
            }
        },
        hiddenChannelContent: function() {
            document.getElementById("left-navigation").style.display = "block";
            document.querySelector(".left-background").style.display = "block";
            rightElement.style.paddingLeft = "120px";
            rightElement.querySelector(".first-level").style.display = "block";
            rightElement.querySelector(".second-level").style.display = "none";
            document.getElementById("filter-channel").style.display = "none";
            channelFilter.exitFilterSection();
        },
        processKeyEvent: function(keyCode) {
            var currentSelectedRow, currentSelectedItem;
            if (!this.filterState) {
                currentSelectedRow = channelGrid.getSelectedRow();
                currentSelectedItem = channelGrid.getSelectedItem();
            }
            var newIndex;
            switch (keyCode) {
                case window.VK_BACK:
                    this.hiddenChannelContent();
                    this.filterState = false;
                    break;
                case window.VK_DOWN:
                    if (this.filterState) {
                        channelFilter.processKeyboard(keyCode);
                    } else {
                        newIndex = currentSelectedRow.index + 1;
                        if (this.filterState) {
                            newIndex = currentSelectedRow.index;
                            this.filterState = false;
                        }
                        channelGrid.setSelectedRowByIndex(newIndex, currentSelectedItem.index);
                    }
                    break;
                case window.VK_ENTER:
                    if (this.filterState) {
                        channelFilter.processKeyboard(keyCode);
                    } else {
                        document.getElementById("prevPage").value = "second-level";
                        channelDetail.showForm(currentSelectedItem.dataId);
                    }
                    break;
                case window.VK_LEFT:
                    if (this.filterState) {
                        channelFilter.processKeyboard(keyCode);
                    } else {
                        newIndex = currentSelectedItem.index - 1;
                        channelGrid.setSelectedItemByIndex(newIndex, currentSelectedRow.index);
                        break;
                    }
                case window.VK_RIGHT:
                    if (this.filterState) {
                        channelFilter.processKeyboard(keyCode);
                    } else {
                        newIndex = currentSelectedItem.index + 1;
                        channelGrid.setSelectedItemByIndex(newIndex, currentSelectedRow.index);
                        break;
                    }
                case window.VK_UP:
                    if (this.filterState) {
                        channelFilter.processKeyboard(keyCode);
                    } else {
                        newIndex = currentSelectedRow.index - 1;
                        if (newIndex >= 0) {
                            channelGrid.setSelectedRowByIndex(newIndex, currentSelectedItem.index);
                        } else {
                            namespace.addClass(document.querySelector(".dropdown-group"), "active");
                            channelFilter.setSelectedFilter(channelFilter.filterType[0]);
                            channelGrid.removeSelectedItems();
                            channelGrid.removeActiveRows();
                            this.filterState = true;
                        }
                    }

                    break;
            }
        }
    }
})(window.FREQ);