(function (namespace) {
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    * LeftMenu.js is file for all of function are execute on Left Navigation
    */
    
    var leftMenu = document.getElementById("left-navigation");
    var leftBackground = document.querySelector(".left-background");
    
    
    namespace.LeftMenu = function() {};
    namespace.LeftMenu.prototype = {
        /*check if left menu is collapse or expand*/
        isActive: function() {
            if (leftMenu.getAttribute("class").indexOf("menu-min") == -1) {
                return true;
            } else {
                return false;
            }
        },
        removeActiveItem: function(element) {
            namespace.removeClass(element, "active");
        },
        /* set new selected menu item when catch event keyUp and keyDown*/
        setNewSelectedItem: function(keyCode) {
            var currentSelect = this.getCurrentSelectedItem();
            if (currentSelect && currentSelect != undefined) {
                if (keyCode == window.VK_UP) {
                    if (currentSelect.previousElementSibling) {
                        this.removeActiveItem(currentSelect);
                        namespace.addClass(currentSelect.previousElementSibling, "active");
                    }
                } else if (keyCode == window.VK_DOWN) {
                    if (currentSelect.nextElementSibling) {
                        this.removeActiveItem(currentSelect);
                        namespace.addClass(currentSelect.nextElementSibling, "active");
                    }
                }
                this.updateRightContent();
            }
        },
        setSelectedFirstElement: function () {
            namespace.removeClass(this.getCurrentSelectedItem(), "active");
            namespace.addClass(leftMenu.firstElementChild, "active");
            this.updateRightContent();
        },
        /* find element and index of selected menu item*/
        getCurrentSelectedItem: function() {
            return leftMenu.querySelector(".active");
        },
        /* Execute collapse Left Navigation when focus on Right Content */
        collapseMenu: function() {
            namespace.addClass(leftMenu, "menu-min");
            leftMenu.style.width = "80px";
            leftBackground.style.width = "80px";
        },
        /* Execute expand Left Navigation when focus on Right Content */
        expandMenu: function() {
            namespace.removeClass(leftMenu, "menu-min");
            leftMenu.style.width = "220px";
            leftBackground.style.width = "220px";
        },
        /* Get Current Menu Item title use for Right content to change content*/
        getCurrentPage: function() {
            var currentSelect = this.getCurrentSelectedItem();
            if (currentSelect != undefined) {
                return currentSelect.getAttribute("tab");
            } else {
                return null;
            }
        },
        updateRightContent: function() {
            /*let textTittle = this.getCurrentSelectedItem().querySelector(".image-text").innerHTML;
            document.querySelector(".seperate").innerHTML = textTittle;*/
            rightContent.updateContentByLeftMenu(this.getCurrentPage());
        }
    }
    
})(window.FREQ);