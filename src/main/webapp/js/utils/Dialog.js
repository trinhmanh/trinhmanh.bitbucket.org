(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    var dialog = document.getElementById("dialog");
    var dialogMessage = dialog.querySelector('.dialog-body > .content-message');
    var dialogButtonRow = dialog.querySelector('.dialog-body > .button-row');

    namespace.Dialog = function() {};
    namespace.Dialog.prototype = {
        message: "",
        constructor: function(message){
            this.message = message;
            dialogMessage.innerHTML = message;
        },
        setContent: function(message) {
            this.message = message;
            dialogMessage.innerHTML = message;
        },
        /**
         * Set buttons for dialog
         * @param buttonStrArr
         */
        setButtons: function (buttonStrArr) {
            if (buttonStrArr && buttonStrArr.length > 0) {
                var i, length = buttonStrArr.length;
                dialogButtonRow.innerHTML = '';
                for (i = 0; i < length; i++) {
                    var btn = document.createElement('button');
                    btn.innerHTML = buttonStrArr[i];
                    dialogButtonRow.appendChild(btn);
                }
            }
        },
        showDialog: function(message) {
            this.setContent(message);
            dialog.querySelector(".dialog-title").innerHTML = "Information Message";
            dialog.querySelector(".button-row").innerHTML = "";
            var button = document.createElement("button");
            button.innerHTML = "OK";
            button.setAttribute("class", "selected");
            button.style.float = "right";
            button.addEventListener("click", this.closeDialog);
            dialog.querySelector(".button-row").appendChild(button);
            dialog.style.display = "block";
        },
        confirmDialog: function (title, message, okBtn, func) {
            this.setContent(message);
            dialog.querySelector(".dialog-title").innerHTML = title;
            dialog.querySelector(".button-row").innerHTML = "";
            var buttonCancel = document.createElement("button");
            buttonCancel.innerHTML = "Cancel";
            buttonCancel.setAttribute("class", "selected");
            buttonCancel.addEventListener("click", this.closeDialog);
            var buttonOK = document.createElement("button");
            buttonOK.innerHTML = okBtn;
            dialog.querySelector(".button-row").appendChild(buttonCancel);
            dialog.querySelector(".button-row").appendChild(buttonOK);
            buttonOK.addEventListener("click", func);
            dialog.style.display = "block";
        },
        closeDialog: function() {
            dialog.style.display = "none";
        },
        isShow: function() {
            return (window.getComputedStyle(dialog, null).display != "none");
        },
        getAllButtons: function () {
            return dialogButtonRow.children;
        },
        isYesNoDialog: function () {
            return dialogButtonRow.children.length > 1;
        },
        /**
         * 0: OK/Yes button
         * 1: Cancel button
         * -1: no selected (wrong case)
         */
        getSelectedButton: function () {
            return dialogButtonRow.querySelector(".selected");
        },
        /**
         * 0: OK/Yes button
         * 1: Cancel button
         * @param index If index out of range buttons's number, do nothing
         */
        setSelectedButton: function (element) {
            namespace.removeClass(dialogButtonRow.querySelector(".selected"), 'selected');
            namespace.addClass(element, 'selected');
        },
        /**
         * Move selected button to the left. Do nothing if it 's the first button
         * @param index
         */
        processLeftKey: function (element) {
            var buttonLast = dialogButtonRow.lastChild;
            var buttonFirst = dialogButtonRow.firstChild;
            if (element == buttonLast) {
                this.setSelectedButton(buttonFirst);
            }
        },
        /**
         * Move selected button to the right. Do nothing if it's the last button
         * @param index
         */
        processRightKey: function (element) {
            var buttonLast = dialogButtonRow.lastChild;
            var buttonFirst = dialogButtonRow.firstChild;
            if (element == buttonFirst) {
                this.setSelectedButton(buttonLast);
            }
        },
        processEnterKey: function (element) {
            element.click();
        },
        /**
         * Handle keyboard events for Dialog. Ignore Right and Left events if there's only one button
         * @param keyCode
         */
        processKeyBoardEvent: function (keyCode) {
            if (!this.isYesNoDialog() && (keyCode == window.VK_RIGHT || keyCode == window.VK_LEFT)) {
                return true;
            }
            var selectedButton = this.getSelectedButton();
            switch (keyCode) {
                case window.VK_RIGHT:
                    this.processRightKey(selectedButton);
                    break;
                case window.VK_LEFT:
                    this.processLeftKey(selectedButton);
                    break;
                case window.VK_ENTER:
                    this.processEnterKey(selectedButton);
                    break;
                case window.VK_BACK:
                    this.closeDialog();
                    break;
            }
        }
    };
})(window.FREQ)