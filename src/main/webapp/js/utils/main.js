(function(namespace){
    /*
    * Author: LongLV
    * CreateDate: 24/08/2016
    */
    window.props = {
        dev_mode: false,
        enable_console_log: true,
        enable_browser_log: false
    };

    var eventsArray = [];
    
    window.bestOfWeb = {};
    window.MP4 = "MP4";
    window.YOUTUBE = "YOUTUBE";
    window.VIMEO = "VIMEO";
    window.HLS = "HLS";
    window.DAILY_MOTION = "DAILY_MOTION";
    window.OTHER = "OTHER";

    window.dialog = new namespace.Dialog();
    window.playerTivo = new namespace.PlayerTiVo();

    window.freService = new namespace.FrequencyAPI();
    window.renderHTML = new namespace.RenderHTML();
    window.leftMenu = new namespace.LeftMenu();
    window.rightContent = new namespace.RightContent();
    window.channel = new namespace.Channel();
    window.channelFilter = new namespace.ChannelFilter();
    window.channelGrid = new namespace.GridData("list-channel-section");
    window.videoGrid = new namespace.GridData("list-video-section");
    window.channelDetail = new namespace.ChannelDetail();
    window.searchForm = new namespace.SearchView();
    window.paginationPlaylist = new namespace.PaginationPlaylist();
    window.favoriteList = [];

    window.videoStop = {};
    window.channelPlayback = {};
    window.isPlayFromChannel = false;
    window.isPlayFromChannelDetails = false;
    window.isPlayFromMainPage = false;

    var guideButton = document.querySelector("#guide .button");
    var guideText = document.querySelector("#guide .text");
/**
 * Extend object with additional object
 * @param destination Object that will be extended
 * @param source Object that will be added to destination
 */

	Object.extend = function(destination, source) {
		for (var property in source) {
			if (source.hasOwnProperty(property)) {
				destination[property] = source[property];
			}
		}
		return destination;
	};
/** 
 * Function that prepares template to later use.
 * @param template Id of a template inside index.html file to parse
 */
	Object.extend(namespace, {
		addClass: function(element, className) {
			if (!namespace.hasClass(element, className) && element) {
				element.className += " " + className;
                element.className = element.className.trim();
			}
		},

		removeClass: function(element, className) {
			if (!element) {
				return;
			}

			element.className = element.className.replace(new RegExp(className, "g"), "").replace(/^ +| +$/g, "").replace(/ +/g, " ");
		},

		hasClass: function(element, className) {
			return element && element.className && element.className.match(className);
		},
		fetchData: function(method, headers, url, data, callback, self) {
			var xhr = new XMLHttpRequest();
			xhr.open(method, url);
			xhr.setRequestHeader("Content-Type", "application/json");
            if (headers && headers.length > 0) {
                var i, leng = headers.length;
                for (i = 0; i < leng; i++) {
                    xhr.setRequestHeader(headers[i].header, headers[i].value);
                }
            }
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status == 200 && callback) {
					callback.call(self, xhr.responseText);
				} else {
                    if (xhr.status != 200) {
                        namespace.unBlockUI();
                        console.log(xhr.readyState + "--" +xhr.status);
                        if (xhr.status == 401) {
                            namespace.fetchData("GET", null, "frequency/auth/device", null, freService.updateAuthenticateInformation);
                        }
                    }
                }
			};
			xhr.send(data);
		},
        fetchDataWithArgs: function(method, headers, url, data, callback, ...args) {
		    var xhr =  new XMLHttpRequest();
            window.currentXHR = xhr;
			xhr.open(method, url);
			xhr.setRequestHeader("Content-Type", "application/json");
            if (headers && headers.length > 0) {
                var i, leng = headers.length;
                for (i = 0; i < leng; i++) {
                    xhr.setRequestHeader(headers[i].header, headers[i].value);
                }
            }
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status == 200 && callback) {
					callback.call(null, xhr.responseText, ...args);
				} else {
                    if (xhr.status != 200) {
                        namespace.hiddenLoadingVideo();
                        console.log(xhr.readyState + "--" +xhr.status);
                    }
                }
			};
			xhr.send(data);
		},

        getCookie: function(key)
        {
          key=key+(namespace.VT_CONFIG.username || "");
          if (document.cookie.indexOf(key)>-1) {
            return document.cookie.match(key+"=([^;]*)")[1];
          } 
        },
        setCookie: function(key,value)
        {

          key=key+(namespace.VT_CONFIG.username || "");
          var date = new Date();
          date.setYear(2300);
          document.cookie = key + "=" + value + "; expires=" + date.toGMTString();
        },
        addEvent: function(element, eventName, functionToCall) {
            if (!eventsArray[element]) {
                eventsArray[element] = {};
            }
            if (!eventsArray[element][eventName]) {
                eventsArray[element][eventName] = [];
                element.addEventListener(eventName, function(e) {
                  for (var i = eventsArray[element][eventName].length - 1; i >= 0; i--){
                    if (!e.cancel) {
                      var entry = eventsArray[element][eventName][i];
                      entry.call(null, e);
                    }
                  }
                },
                true);
            }

            eventsArray[element][eventName].push(functionToCall);
        },
        removeEvent: function(element, eventName, functionToCall) {
            if (eventsArray[element] && eventsArray[element][eventName]) {
                var arr = eventsArray[element][eventName];
                var index = arr.indexOf(functionToCall);
                if (index > - 1) {
                    arr.splice(index, 1);
                }
            }
        },
        runningInBrowserOrEmulator: function(){
            var ua = navigator.userAgent;
            if (   ua.indexOf('Model') == -1
                || ua.indexOf('Model/Opera-TvEmulator') != -1
                || ua.indexOf('Model/Opera-Webkit') != -1){
                    return true;
            } else {
                return false;
            }
        },
        cssProperty: function(element, property) {
            if (element) {
                return window.getComputedStyle(element, null).getPropertyValue(property);
            } else {
                throw "Cannot read property of undefined";
            }
            
        },
        convertSecondsToMinSec: function(currentTime) {
            var mins = Math.floor(currentTime / 60);
            var seconds = Math.floor(currentTime - mins * 60);
            return (mins < 10 ? '0' : '') + mins + ':' + (seconds < 10 ? '0' : '') + seconds;
        },
        getVideoType: function(url) {
            if (/.mp4$/.test(url)) {
                return MP4;
            } else if (/youtube.com/.test(url)) {
                return YOUTUBE;
            } else if (/vimeo.com/.test(url)) {
                return VIMEO;
            } else if (/dailymotion.com/.test(url)) {
                return DAILY_MOTION;
            } else if (/m3u8/.test(url)) {
                return HLS;
            } else {
                return OTHER;
            }
        },
        pluralize: function(num, noun) {
            return (num > 1) ? (noun + 's') : noun;
        },
        calculateTimeAgo: function(datePast) {
            var past = new Date(datePast);
            var now = new Date();
            var diffYear = now.getFullYear() - past.getFullYear();
            if (diffYear > 0) {
                return diffYear + ' ' + this.pluralize(diffYear, 'year') + ' ago';
            } else {
                var diffMonth = now.getFullYear() * 12 + now.getMonth() - (past.getFullYear() * 12 + past.getMonth());
                if (diffMonth > 0) {
                    return diffMonth + ' ' + this.pluralize(diffMonth, 'month') + ' ago';
                } else {
                    var diffTime = now.getTime() - past.getTime();
                    var diffWeek = parseInt(diffTime / (24 * 3600 * 1000 * 7));
                    if (diffWeek > 0) {
                        return diffWeek + ' ' + this.pluralize(diffWeek, 'week') + ' ago';
                    } else {
                        var diffDay = parseInt(diffTime / (24 * 3600 * 1000));
                        if (diffDay > 0) {
                            return diffDay + ' ' + this.pluralize(diffDay, 'day') + ' ago';
                        } else {
                            var diffHour = parseInt(diffTime / (3600 * 1000));
                            if (diffHour > 0) {
                                return diffHour + ' ' + this.pluralize(diffHour, 'hour') + ' ago';
                            } else {
                                var diffMin = parseInt(diffTime / (60 * 1000));
                                return diffMin + ' ' + this.pluralize(diffMin, 'min') + ' ago';
                            }
                        }
                    }
                }
            }
        },
        hiddenLoadingVideo: function () {
            document.querySelector(".loading-video").style.display = "none";
            document.querySelector(".loading-video").style.background = "#000";
        },
        loadingVideo: function (mode) {
            if (mode) {
                document.querySelector(".loading-video").style.background = "transparent";
            } else {
                document.querySelector(".loading-video").style.background = "#000";
            }
            document.querySelector(".loading-video").style.display = "block";
        },
        blockUI: function(isService) {
            var loadingModalElement = document.querySelector(".loading-modal");
            if (isService) {
                loadingModalElement.querySelector(".brand").style.display = "none";
                loadingModalElement.querySelector(".loading-icon").setAttribute("src", "img/loadingservice.gif");
                loadingModalElement.querySelector(".loading-icon").setAttribute("width", "40");
                loadingModalElement.querySelector(".loading-icon").setAttribute("height", "40");
            } else {
                loadingModalElement.querySelector(".brand").style.display = "block";
                loadingModalElement.querySelector(".loading-icon").setAttribute("src", "img/loading.gif");
                loadingModalElement.querySelector(".loading-icon").setAttribute("width", "200");
                loadingModalElement.querySelector(".loading-icon").setAttribute("height", "200");
            }
            loadingModalElement.style.display = "block";
        },
        unBlockUI: function() {
            document.querySelector(".loading-modal").style.display = "none";
        },
        debug: function (message) {
            if (window.props.enable_browser_log) {
                document.getElementById("debug").innerHTML = message;
            }
        },
        appendDebug: function (message) {
            if (window.props.enable_browser_log) {
                document.getElementById("debug").innerHTML += message;
            }
        },
        /**
         * Show notification message on screen then hide it
         */
        notify: function (message) {
            var notification = document.getElementById("notification");
            notification.querySelector(".notification-message").innerHTML = message;
            notification.classList.remove("fade-in-out");
            void notification.offsetWidth;
            notification.classList.add("fade-in-out");
        },
        /**
         * Convert Frequency Service 's response data to VideoDTO
         * @param data
         */
        convertResponseData2VideoDTO: function (data) {
            var videoDTO = new namespace.VideoDTO();
            videoDTO.videoId = data.video_id;
            videoDTO.title = data.title;
            videoDTO.videoURL = (data.media_url.match(/youtube.com/g) != null) ? data.source_url : data.media_url;
            if (data.channel) {
                videoDTO.channelId = data.channel.channel_id;
                videoDTO.channelTitle = data.channel.title
            }
            videoDTO.imgURL = data.image_url;
            videoDTO.datePublish = data.date_publish;
            return videoDTO;
        },
        /**
         * Return true if videoId is in favourite list
         */
        isFavouritedVideo: function (videoId) {
            return (videoId) ? favoriteList.indexOf(videoId) != -1 : false;
        },
        /**
         * Control guide information to consistent with video status (favourite or not)
         */
        setGuideFavourite: function (isFavourite) {
            if (isFavourite) {
                guideButton.style.background = "url(img/thumb-down.png)";
                guideButton.style.backgroundSize = "17px 17px";
                guideButton.style.backgroundRepeat = "no-repeat";
                guideText.innerHTML = "remove video from";
            } else {
                guideButton.style.background = "url(img/thumb-up.png)";
                guideButton.style.backgroundSize = "17px 17px";
                guideButton.style.backgroundRepeat = "no-repeat";
                guideText.innerHTML = "save video to";
            }
        },
        /**
         * Store current video information to memory (videoId, playing or not)
         */
        storeCurrentVideo: function (videoId, isPlaying) {
            window.videoStop = {};
            window.videoStop.videoId = videoId;
            window.videoStop.isPlaying = isPlaying;
            window.videoStop.currentTime = player.getCurrentTime();
            console.log("Store current video info to memory: " + JSON.stringify(window.videoStop));
        },
        clearCurrentVideo: function () {
            window.videoStop = null;
        },
        /**
         * Check if videoId is current video or not
         */
        isCurrentVideo: function (videoId) {
            return window.videoStop && window.videoStop.videoId === videoId;
        },
        /**
         * Check if current video is playing or not
         */
        isCurrentVideoPlaying: function () {
            return window.videoStop && window.videoStop.isPlaying;
        },
        log: function (message) {
            if (window.props.enable_console_log) {
                console.log(message);
            }
        },
        restartAnimation: function(elem, animationClass) {
            elem.classList.remove(animationClass);
            void elem.offsetWidth;
            elem.classList.add(animationClass);
        },
        stopAnimation: function(elem, animationClass) {
            elem.classList.remove(animationClass);
        },
        markPlayingFirstVideoInChannel: function () {
            window.isPlayFromChannel = true;
            window.isPlayFromChannelDetails = window.isPlayFromMainPage = false;
        },
        markPlayingFromChannelDetails: function () {
            window.isPlayFromChannel = window.isPlayFromMainPage = false;
            window.isPlayFromChannelDetails = true;
        },
        markPlayingFromMainPage: function () {
            window.isPlayFromMainPage = true;
            window.isPlayFromChannel = window.isPlayFromChannelDetails = false;
        }
        });
})(window.FREQ);