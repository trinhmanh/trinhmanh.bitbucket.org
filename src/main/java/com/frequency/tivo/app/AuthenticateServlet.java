package com.frequency.tivo.app;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.frequency.tivo.app.util.*;

/**
 * Servlet implementation class AuthenticateServlet
 */
public class AuthenticateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(AuthenticateServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthenticateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("Entering AuthenticateServlet.");
		
		JSONObject requestBody = new JSONObject();
		requestBody.put("os", "iOS");
		requestBody.put("os_version", "7.1");
		requestBody.put("model", "iPad");
		requestBody.put("manufacturer", "Apple");
		requestBody.put("height", "768");
		requestBody.put("width", "1024");
		requestBody.put("app_version", "2.3.1 build(325)");
		requestBody.put("company", "Frequency");
		requestBody.put("key", ConfigurationUtils.getProperty("frequency.key"));
		requestBody.put("app_name", ConfigurationUtils.getProperty("frequency.appname"));

		Map<String,String> requestHeader = new HashMap<>();
		requestHeader.put("Content-Type", "application/json");

		JSONObject data = HttpClientUtils.doPost(ConfigurationUtils.getProperty("frequency.auth.device.url"), requestBody.toString(), requestHeader);
		logger.debug(data.toString());
		
		response.getWriter().append(data.get("bodyContent").toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
