package com.frequency.tivo.app.util;

import java.io.IOException;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public final class HttpClientUtils {

	private HttpClientUtils() {
	}

	public static JSONObject doPost(String url, String bodyContent, Map<String, String> header) {
		HttpClient httpclient = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(url);
		JSONObject result = new JSONObject();
		try {
			StringEntity requestBody = new StringEntity(bodyContent);
			request.setEntity(requestBody);
			for (Map.Entry<String, String> entry : header.entrySet()) {
				request.setHeader(entry.getKey(), entry.getValue());
			}
			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity);
			result.put("status", response.getStatusLine().getStatusCode());
			result.put("bodyContent", new JSONObject(responseString));
		} catch (ClientProtocolException e) {
			result.put("status", "500");
			result.put("bodyContent", "");
			e.printStackTrace();
		} catch (IOException e) {
			result.put("status", "500");
			result.put("bodyContent", "");
			e.printStackTrace();
		} finally {
			request.releaseConnection();
		}
		return result;
	}
	
	public static JSONObject doGet(String url, Map<String, String> header) {
		HttpClient httpclient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);
		JSONObject result = new JSONObject();
		try {
			for (Map.Entry<String, String> entry : header.entrySet()) {
				request.setHeader(entry.getKey(), entry.getValue());
			}
			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity);
			result.put("status", response.getStatusLine().getStatusCode());
			result.put("bodyContent", new JSONObject(responseString));
		} catch (ClientProtocolException e) {
			result.put("status", "500");
			result.put("bodyContent", "");
			e.printStackTrace();
		} catch (IOException e) {
			result.put("status", "500");
			result.put("bodyContent", "");
			e.printStackTrace();
		} finally {
			request.releaseConnection();
		}
		return result;
	}
}
