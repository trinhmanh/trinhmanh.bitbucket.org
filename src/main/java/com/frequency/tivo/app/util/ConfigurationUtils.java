package com.frequency.tivo.app.util;

import java.io.InputStream;
import java.util.Properties;


/**
 * Configuration Utils.
 * @author thuanlv1
 *
 */
public final class ConfigurationUtils {
	
	/**
	 * logger.
	 */
	
	/**
	 * file name.
	 */
	private static final String APPLICATION_PROP_FILE = "tivo.properties";
	
	/**
	 * properties.
	 */
	private static Properties properties;
	
	/**
	 * defaul contructor.
	 */
	private ConfigurationUtils() {
	}
	
	/**
	 * Load property.
	 * @return Properties
	 */
	private static Properties getProperties() {
		if (properties == null) {
			try {
				properties = new Properties();
				InputStream is = ConfigurationUtils.class.getClassLoader().getResourceAsStream(APPLICATION_PROP_FILE);
				if (is == null) {
					System.out.println("Cannot found application resource file!");
				}
				properties.load(is);
			    is.close(); 
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return properties;
	}
	
	/**
	 * Get property.
	 * @param key key
	 * @return value
	 */
	public static String getProperty(String key) {
		if(getProperties().getProperty(key) == null){
			return "";
		}
		return getProperties().getProperty(key);
	}
	
}

