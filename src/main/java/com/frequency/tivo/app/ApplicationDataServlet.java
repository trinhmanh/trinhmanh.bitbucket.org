package com.frequency.tivo.app;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.frequency.tivo.app.util.ConfigurationUtils;
import com.frequency.tivo.app.util.HttpClientUtils;

/**
 * Servlet implementation class ApplicationDataServlet
 */
public class ApplicationDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	final static Logger logger = Logger.getLogger(ApplicationDataServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApplicationDataServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> requestHeader = new HashMap<>();
		requestHeader.put("Content-Type", "application/json");
		JSONObject data = HttpClientUtils.doGet(ConfigurationUtils.getProperty("frequency.layout.url"), requestHeader);
		
		response.getWriter().append(data.get("bodyContent").toString().toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
